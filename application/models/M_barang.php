<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_barang extends CI_Model {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function list_barang()
	{
		$sql = "select master_barang.id, master_barang.kode_barang, master_barang.nama_barang, 
        master_jenis.nama_jenis as kategori ,master_barang.stok, master_satuan.nama_satuan as satuan,
        master_barang.harga_modal
        from master_barang LEFT JOIN master_jenis on master_barang.kategori = master_jenis.id 
        LEFT JOIN master_satuan on master_barang.satuan = master_satuan.id";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
	}
	
	public function list_barang_habis()
	{
		$sql = "select master_barang.id, master_barang.kode_barang, master_barang.nama_barang, 
        master_jenis.nama_jenis as kategori ,master_barang.stok, master_satuan.nama_satuan as satuan,
        master_barang.harga_modal
        from master_barang LEFT JOIN master_jenis on master_barang.kategori = master_jenis.id 
        LEFT JOIN master_satuan on master_barang.satuan = master_satuan.id WHERE master_barang.stok < 100";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
    
    public function hapus_barang($id)
    {
        $sql = "delete from master_barang where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        return $queryRec;
	}

	public function get_product($id)
	{
		$sql = "select * from master_barang where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $query = $this->db->query($sql, array($id));
		if ($query->num_rows() == 1) {
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return FALSE;
		}
	}

	public function get_category()
	{
		$sql = "select * from master_jenis";
		$queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
	}

	public function get_satuan()
	{
		$sql = "select * from master_satuan";
		$queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
	}

	public function save_edit($id,$nama_barang,$kode_barang,$kategori,$stok,$satuan,$harga_modal)
	{
		$sql = "update master_barang set kode_barang=?, nama_barang=?, kategori=?, stok=?, satuan=?, harga_modal=? where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($kode_barang, $nama_barang, $kategori,$stok, $satuan, $harga_modal, $id));
        return $queryRec;
	}

	public function save_barang($supplier,$nama_barang,$kode_barang,$kategori,$stok,$satuan,$harga_modal)
	{
		$sql = "INSERT INTO `master_barang`(`id`,`kode_barang`, `nama_barang`, `kategori`, `stok`, `satuan`, `harga_modal`, `supplier`)  values (null,?,?,?,?,?,?,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($kode_barang, $nama_barang, $kategori, $stok, $satuan, $harga_modal, $supplier));
        return $queryRec;
	}
	
}