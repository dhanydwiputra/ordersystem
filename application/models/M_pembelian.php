<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_pembelian extends CI_Model {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function get_supplier()
	{
		$sql = "select * from master_supplier";
		$queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
    
    function get_barang($id){
        $hasil=$this->db->query("SELECT * FROM master_barang WHERE supplier='$id'");
        return $hasil->result();
	}
	
	function get_kode($id){
        $hasil=$this->db->query("SELECT * FROM master_barang WHERE kode_barang='$id'");
        return $hasil->result();
	}
	
	function save($no_referensi,$supplier,$tanggal,$gt) {
		$sql = "INSERT INTO `pembelian_supplier`(`id`, `no_referensi`, `id_supplier`, `tanggal`, `total`, `status`)  values (null,?,?,?,?,0)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($no_referensi,$supplier,$tanggal,$gt));
        return $queryRec;
	}

	function save_detail($no_referensi, $kode_barang, $qty, $price, $grandtotal) {
		$sql = "INSERT INTO `detail_pembelian_supplier`(`id`, `no_referensi`, `kode_barang`, `qty`, `harga_satuan`, `total`)  values (null,?,?,?,?,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
		$sql2 = "update master_barang set stok = (stok+?) where kode_barang = ?";
		$queryRec = $this->db->query($sql, array($no_referensi, $kode_barang, $qty, $price, $grandtotal));
		$queryRec2 = $this->db->query($sql2, array($qty, $kode_barang));
        return $queryRec;
	}

}