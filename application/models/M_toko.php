<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_toko extends CI_Model {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function list_toko ()
	{
		$sql = "select a.id, a.nama_toko, b.nama_daerah, c.nama_daerah as nama_daerah_user from master_toko a 
		left join master_daerah b on a.daerah = b.id
		left join master_daerah_user c on a.daerah_user = c.id";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
	}

	public function get_daerah()
	{
		$sql = "select * from master_daerah";
		$queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
	}

	public function save_toko($nama_toko,$daerah)
	{
		$sql = "INSERT INTO `master_toko`(`id`,`nama_toko`, `daerah`, `daerah_user`)  values (null,?,?,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_toko, $daerah, $daerah_user));
        return $queryRec;
	}

	public function hapus_toko($id)
    {
        $sql = "delete from master_toko where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        return $queryRec;
	}

	public function get_toko($id)
	{
		$sql = "select * from master_toko where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $query = $this->db->query($sql, array($id));
		if ($query->num_rows() == 1) {
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return FALSE;
		}
	}

	public function save_edit_toko($id,$nama_toko,$daerah, $daerah_user)
	{
		$sql = "update master_toko set nama_toko=?, daerah=?, daerah_user = ? where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_toko, $daerah, $daerah_user, $id));
        return $queryRec;
	}

	public function save_daerah($nama_daerah)
	{
		$sql = "INSERT INTO `master_daerah`(`id`,`nama_daerah`)  values (null,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_daerah));
        return $queryRec;
	}

	public function hapus_daerah($id)
    {
        $sql = "delete from master_daerah where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        return $queryRec;
	}
	
	public function get_daerah_edit($id){
		$sql = "select * from master_daerah where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $query = $this->db->query($sql, array($id));
		if ($query->num_rows() == 1) {
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return FALSE;
		}
	}

	public function save_edit_daerah($id,$nama_daerah)
	{
		$sql = "update master_daerah set nama_daerah=? where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_daerah, $id));
        return $queryRec;
	}

	public function get_daerah_user()
	{
		$sql = "select * from master_daerah_user";
		$queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
	
}