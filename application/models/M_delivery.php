<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_delivery extends CI_Model {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function show_pesanan ()
	{
		$sql = "select a.* , b.nama_toko from pesanan_barang a left join master_toko b on a.id_toko = b.id where status = 0";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
    
	function save_do($no_do,$driver,$tanggal,$no_gudang,$no_kendaraan) {
		$sql = "INSERT INTO `delivery_order`(`id`,`no_do`, `driver`, `tanggal`, `no_gudang`,`no_kendaraan`)  values (null,?,?,?,?,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($no_do,$driver,$tanggal,$no_gudang,$no_kendaraan));
        return $queryRec;
    }
    
    function save_detail_do($no_do,$kode_pesanan) {
		$sql = "INSERT INTO `detail_do`(`id`,`no_do`, `kode_pesanan`)  values (null,?,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($no_do,$kode_pesanan));
        return $queryRec;
    }
    
    public function ubah_status_pesanan($id)
	{
		$sql_update = "update pesanan_barang set status=2 where kode_pesanan = ?";
        $queryUpdate = $this->db->query($sql_update, array($id));
        return $queryRec;
    }
    
    public function show_all_do ()
	{
		$sql = "select * from delivery_order";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }

    public function get_delivery($id)
    {
        $sql = "select * from delivery_order where no_do = ?";
        $query = $this->db->query($sql,array($id))->row_array();
        return $query;
    }

    public function list_delivery($id)
	{
		$sql = "select a.id, a.kode_pesanan, b.id_toko, c.nama_toko from detail_do a left join pesanan_barang b
        on a.kode_pesanan = b.kode_pesanan left join master_toko c on b.id_toko = c.id
        where a.no_do = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id))->result_array();
        return $queryRec;
    }
    
    function get_kode_do($date) {
        $sql = "select count(0) as kode_pesanan from delivery_order";
		$queryRec = $this->db->query($sql, array($date))->row_array();
        return $queryRec['kode_pesanan'];
    }
    
    public function hapus_do($id)
    {
        $sql = "delete from delivery_order where no_do = ?";
        $sql2 = "delete from detail_do where no_do = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        $queryRec2 = $this->db->query($sql2, array($id));
        return $queryRec;
    }
    
    public function ubah_status_pesanan_batal($id)
	{
		$sql_update = "update pesanan_barang set status=0 where kode_pesanan = ?";
        $queryUpdate = $this->db->query($sql_update, array($id));
        return $queryRec;
    }

    public function list_delivery_order($id)
	{
		$sql = "select f.nama_daerah, g.nama_barang, sum(d.qty) as qty
        from delivery_order a 
            left join detail_do b
                on a.no_do = b.no_do
            left join pesanan_barang c
                on b.kode_pesanan = c.kode_pesanan
            left join detail_pesanan_barang d
                on c.kode_pesanan = d.kode_pesanan
            left join master_toko e
                on c.id_toko = e.id
            left join master_daerah f
                on e.daerah = f.id
            left join master_barang g
                on d.kode_barang = g.kode_barang
        where a.no_do = ?
        group by f.nama_daerah, g.nama_barang
        order by f.nama_daerah";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id))->result_array();
        return $queryRec;
    }

}