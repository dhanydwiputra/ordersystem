<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_pesanan extends CI_Model {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function get_toko($daerah)
	{
		$sql = "select * from master_toko where daerah_user = ?";
		$queryRec = $this->db->query($sql,array($daerah))->result_array();
        return $queryRec;
    }

    public function get_supplier()
	{
		$sql = "select * from master_supplier";
		$queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
    
    function get_barang($id){
        $hasil=$this->db->query("SELECT * FROM master_barang WHERE supplier='$id'");
        return $hasil->result();
	}
	
	function get_kode($id){
        $hasil=$this->db->query("SELECT * FROM master_barang WHERE kode_barang='$id'");
        return $hasil->result();
	}
	
	function save_pesanan($kode_pesanan,$toko,$tanggal,$gt, $user) {
		$sql = "INSERT INTO `pesanan_barang`(`id`, `kode_pesanan`, `id_toko`, `tanggal_pesanan`, `total`, `status`, `id_user`)  values (null,?,?,?,?,0,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($kode_pesanan,$toko,$tanggal,$gt, $user));
        return $queryRec;
	}

	function save_detail($kode_pesanan, $kode_barang, $qty, $price, $grandtotal) {
		$sql = "INSERT INTO `detail_pesanan_barang`(`id`, `kode_pesanan`, `kode_barang`, `qty`, `harga_satuan`, `total`)  values (null,?,?,?,?,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
		$sql2 = "update master_barang set stok = (stok-?) where kode_barang = ?";
		$queryRec = $this->db->query($sql, array($kode_pesanan, $kode_barang, $qty, $price, $grandtotal));
		$queryRec2 = $this->db->query($sql2, array($qty, $kode_barang));
        return $queryRec;
    }
    
    function get_kode_pesanan($date) {
        // $sql = "select IFNULL(max(kode_pesanan),0) as kode_pesanan from pesanan_barang where tanggal_pesanan=?";
		// $queryRec = $this->db->query($sql, array($date))->row_array();
		// return $queryRec['kode_pesanan'];
		$sql = "select COUNT(0) as jumlah from pesanan_barang where tanggal_pesanan like ?";
		$queryRec = $this->db->query($sql, array($date))->row_array();
		return $queryRec['jumlah'];
	}

	function get_kode_pesanan_max($date) {
        // $sql = "select IFNULL(max(kode_pesanan),0) as kode_pesanan from pesanan_barang where tanggal_pesanan=?";
		// $queryRec = $this->db->query($sql, array($date))->row_array();
		// return $queryRec['kode_pesanan'];
		$sql = "select max(kode_pesanan) as last from pesanan_barang where tanggal_pesanan like ?";
		$queryRec = $this->db->query($sql, array($date))->row_array();
		return $queryRec['last'];
	}
	
	public function show_pesanan ()
	{
		$sql = "select a.* , b.nama_toko from pesanan_barang a left join master_toko b on a.id_toko = b.id where status = 0 or status = 2";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
	}

	public function show_pesanan_sales ($id_daerah)
	{
		$sql = "select a.* , b.nama_toko from pesanan_barang a left join master_toko b on a.id_toko = b.id where b.daerah_user = ? and status = 0 or status = 2";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql,array($id_daerah))->result_array();
        return $queryRec;
	}

	public function show_pesanan_done ()
	{
		$sql = "select a.* , b.nama_toko from pesanan_barang a left join master_toko b on a.id_toko = b.id where status = 1";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
	}

	public function show_pesanan_done_sales ($id_daerah)
	{
		$sql = "select a.* , b.nama_toko from pesanan_barang a left join master_toko b on a.id_toko = b.id where b.daerah_user = ? and status = 1";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id_daerah))->result_array();
        return $queryRec;
	}


	public function get_pesanan($id)
    {
        $sql = "select a.*, b.nama_toko, c.nama_daerah, d.username from pesanan_barang a left join master_toko b on a.id_toko = b.id left join master_daerah c on b.daerah = c.id left join master_user d on a.id_user = d.id where a.kode_pesanan = ?";
        $query = $this->db->query($sql,array($id))->row_array();
        return $query;
    }

    public function list_pesanan($id)
	{
		$sql = "select a.*, b.*, c.nama_satuan from detail_pesanan_barang a left join master_barang b on a.kode_barang = b.kode_barang left join master_satuan c on b.satuan = c.id where kode_pesanan=?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id))->result_array();
        return $queryRec;
	}
	
	public function hapus_pesanan($id)
    {
        $sql = "delete from pesanan_barang where kode_pesanan = ?";
        $sql2 = "delete from detail_pesanan_barang where kode_pesanan = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        $queryRec2 = $this->db->query($sql2, array($id));
        return $queryRec;
	}

	public function get_pesanan_hapus($id)
	{
		$sql = "select * from detail_pesanan_barang where kode_pesanan = ?";
		$queryRec = $this->db->query($sql, array($id))->result_array();
		return $queryRec;
	}

	public function back_stock($id, $stock)
	{
		$sql2 = "update master_barang set stok = (stok+?) where kode_barang = ?";
		$queryRec2 = $this->db->query($sql2, array($stock, $id));
		return $queryRec2;
	}
	
	public function selesaikan_pesanan($id)
	{
		$sql_update = "update pesanan_barang set status=1 where kode_pesanan = ?";
        $queryUpdate = $this->db->query($sql_update, array($id));
        return $queryRec;
	}

	public function batal_selesai($id)
	{
		$sql_update = "update pesanan_barang set status=0 where kode_pesanan = ?";
        $queryUpdate = $this->db->query($sql_update, array($id));
        return $queryRec;
	}

	public function batal_do($id)
	{
		$sql_update = "update pesanan_barang set status=0 where kode_pesanan = ?";
        $queryUpdate = $this->db->query($sql_update, array($id));
        return $queryRec;
	}

	public function hapus_detail_pesanan($kode, $id, $total, $qty)
    {
		$sql1 = "delete from detail_pesanan_barang where kode_pesanan = ? and kode_barang=?";
		$sql2 = "update pesanan_barang set total = (total-?) where kode_pesanan = ?";
		$sql3 = "update master_barang set stok = (stok+?) where kode_barang = ?";
		$queryRec1 = $this->db->query($sql1, array($kode ,$id));
		$queryRec2 = $this->db->query($sql2, array($total ,$kode));
		$queryRec3 = $this->db->query($sql3, array($qty ,$id));
        return $queryRec;
	}

}