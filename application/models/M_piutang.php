<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_piutang extends CI_Model {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function list_piutang()
	{
		// $sql = "select a.id, a.nama_toko, 
        // (ifnull(sum(b.total),0))+(select IFNULL(z.jumlah_hutang,0) from master_toko x left join hutang_persisten z on x.id = z.id_toko where x.id = a.id) as `total_hutang`, 
        // IFNULL(SUM(c.total_bayar),0) as jumlah_pembayaran from master_toko a 
        // left join (select * from pesanan_barang where status = 1) b on a.id = b.id_toko
        // left join pembayaran_toko c on a.id = c.id_toko
        // GROUP BY a.nama_toko";
        $sql = "select a.id, a.nama_toko, 
        (ifnull(sum(b.total),0))+(select IFNULL(z.jumlah_hutang,0) from master_toko x left join hutang_persisten z on x.id = z.id_toko where x.id = a.id) as `total_hutang`, 
        (select IFNULL(SUM(total_bayar),0) from pembayaran_toko where id_toko = a.id) as jumlah_pembayaran
        from master_toko a left join (select * from pesanan_barang where status = 1) b on a.id = b.id_toko
        GROUP BY a.nama_toko";
		$queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }

    public function list_piutang_sales($id_daerah)
	{
		// $sql = "select a.id, a.nama_toko, 
        // (ifnull(sum(b.total),0))+(select IFNULL(z.jumlah_hutang,0) from master_toko x left join hutang_persisten z on x.id = z.id_toko where x.id = a.id) as `total_hutang`, 
        // IFNULL(SUM(c.total_bayar),0) as jumlah_pembayaran from master_toko a 
        // left join (select * from pesanan_barang where status = 1) b on a.id = b.id_toko
        // left join pembayaran_toko c on a.id = c.id_toko
        // GROUP BY a.nama_toko";
        $sql = "select a.id, a.nama_toko, 
        (ifnull(sum(b.total),0))+(select IFNULL(z.jumlah_hutang,0) from master_toko x left join hutang_persisten z on x.id = z.id_toko where x.id = a.id) as `total_hutang`, 
        (select IFNULL(SUM(total_bayar),0) from pembayaran_toko where id_toko = a.id) as jumlah_pembayaran
        from master_toko a left join (select * from pesanan_barang where status = 1) b on a.id = b.id_toko
        where a.daerah_user = ?
        GROUP BY a.nama_toko";
		$queryRec = $this->db->query($sql, array($id_daerah))->result_array();
        return $queryRec;
    }

    public function detail_piutang($id_toko)
	{
		$sql = "select id_toko, 'Pembelian' as `type`,tanggal_pesanan as tanggal , total, id as id from pesanan_barang where status = 1 and id_toko = ?
        UNION ALL
        select id_toko, 'Pembayaran' as `type`, tanggal, total_bayar as total, id as id from pembayaran_toko where id_toko = ?
        UNION ALL
        select id_toko, 'Hutang Sebelumnya' as `type`, tanggal, jumlah_hutang as total, id as id from hutang_persisten where id_toko = ?
        ORDER BY tanggal desc";
		$queryRec = $this->db->query($sql,array($id_toko,$id_toko,$id_toko))->result_array();
        return $queryRec;
    }

    public function detail_piutang_limit($id_toko)
	{
		$sql = "select id_toko, 'Pembelian' as `type`,tanggal_pesanan as tanggal , total from pesanan_barang where status = 1 and id_toko = ?
        UNION ALL
        select id_toko, 'Pembayaran' as `type`, tanggal, total_bayar as total from pembayaran_toko where id_toko = ?
        UNION ALL
        select id_toko, 'Hutang Sebelumnya' as `type`, tanggal, jumlah_hutang as total from hutang_persisten where id_toko = ?
        ORDER BY tanggal desc limit 10";
		$queryRec = $this->db->query($sql,array($id_toko,$id_toko,$id_toko))->result_array();
        return $queryRec;
    }

    public function list_toko()
    {
        $sql = "select * from master_toko";
		$queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }

    function get_kode_pembayaran($date) {
        $sql = "select IFNULL(max(kode_pembayaran),0) as kode_pembayaran from pembayaran_toko where tanggal=?";
		$queryRec = $this->db->query($sql, array($date))->row_array();
        return $queryRec['kode_pembayaran'];
    }
    
    public function save_pembayaran($kode_pembayaran,$toko,$tanggal,$jumlah_bayar,$userid)
    {
        $sql = "INSERT INTO `pembayaran_toko`(`id`,`kode_pembayaran`, `id_toko`, `tanggal`, `id_user`,`total_bayar`)  values (null,?,?,?,?,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($kode_pembayaran, $toko, $tanggal, $userid, $jumlah_bayar));
        return $queryRec;
    }

    public function hapus_pembayaran($id)
    {
        $sql = "delete from pembayaran_toko where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        return $queryRec;
    }
    
    function header_piutang($nama_toko) {
        $sql = "select b.nama_daerah from master_toko a left join master_daerah b on a.daerah = b.id where nama_toko = ?";
		$queryRec = $this->db->query($sql, array(str_replace('%20',' ', $nama_toko)))->row_array();
        return $queryRec['nama_daerah'];
    }

}