<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_hutang extends CI_Model {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function list_hutang()
	{
		$sql = "select a.*, b.nama_supplier from pembelian_supplier a left join master_supplier b on a.id_supplier = b.id where a.status = 0";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }

    public function list_hutang_lunas()
	{
		$sql = "select a.*, b.nama_supplier from pembelian_supplier a left join master_supplier b on a.id_supplier = b.id where a.status = 1";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
    
    public function total_hutang()
    {
        $sql = "select sum(total) as total from pembelian_supplier where status = 0";
        $query = $this->db->query($sql)->row_array();
        return $query['total'];
    }

    public function get_hutang($id)
    {
        $sql = "select a.*, b.nama_supplier from pembelian_supplier a left join master_supplier b on a.id_supplier = b.id where a.no_referensi = ?";
        $query = $this->db->query($sql,array($id))->row_array();
        return $query;
    }

    public function list_pembelian($id)
	{
		$sql = "select a.*, b.* from detail_pembelian_supplier a left join master_barang b on a.kode_barang = b.kode_barang where no_referensi=?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id))->result_array();
        return $queryRec;
    }
    

    public function save_pembayaran($no_referensi,$tanggal,$jumlah_bayar,$userid)
	{
        $sql = "INSERT INTO `pembayaran_supplier`(`id`,`no_referensi`, `tanggal_pembayaran`, `jumlah_dibayar`, `id_user`)  values (null,?,?,?,?)";
        $sql_update = "update pembelian_supplier set status=1 where no_referensi = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($no_referensi, $tanggal, $jumlah_bayar, $userid));
        $queryUpdate = $this->db->query($sql_update, array($no_referensi));
        return $queryRec;
    }
    
    public function list_pembayaran($id)
	{
		$sql = "select a.*, b.* from pembayaran_supplier a left join master_user b on a.id_user = b.id where a.no_referensi = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id))->result_array();
        return $queryRec;
    }

    public function hapus_pembelian($id)
    {
        $sql = "delete from pembelian_supplier where no_referensi = ?";
        $sql2 = "delete from detail_pembelian_supplier where no_referensi = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        $queryRec2 = $this->db->query($sql2, array($id));
        return $queryRec;
    }
    
    public function batal_pelunasan($id)
    {
        $sql = "update pembelian_supplier set status=0 where no_referensi = ?";
        $sql2 = "delete from pembayaran_supplier where no_referensi = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        $queryRec2 = $this->db->query($sql2, array($id));
        return $queryRec;
	}
	
}