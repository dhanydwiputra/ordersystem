<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_master extends CI_Model {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function list_user ()
	{
		$sql = "select a.*, b.nama_role from master_user a left join role b on a.role = b.id_role";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
    
    public function get_user($id)
	{
		$sql = "select * from master_user where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $query = $this->db->query($sql, array($id));
		if ($query->num_rows() == 1) {
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return FALSE;
		}
    }
    
    public function save_edit_user($id,$username,$email, $password, $daerah, $role)
	{
		$sql = "update master_user set username=?, email=?, password=?, role=?, daerah=? where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($username, $email, md5($password), $role, $daerah, $id));
        return $queryRec;
    }
    
    public function save_user($username,$email,$password,$daerah, $role)
	{
		$sql = "INSERT INTO `master_user`(`id`,`username`, `email`, `password`, `role`, `daerah`)  values (null,?,?,?,?,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($username, $email, md5($password), $role, $daerah));
        return $queryRec;
	}
    
    public function hapus_user($id)
    {
        $sql = "delete from master_user where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        return $queryRec;
    }
    
    public function get_daerah_user()
	{
		$sql = "select * from master_daerah_user";
		$queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }

    public function get_role()
	{
		$sql = "select * from role";
		$queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
    
    // 

    public function list_satuan ()
	{
		$sql = "select * from master_satuan";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
    
    public function get_satuan($id)
	{
		$sql = "select * from master_satuan where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $query = $this->db->query($sql, array($id));
		if ($query->num_rows() == 1) {
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return FALSE;
		}
    }
    
    public function save_edit_satuan($id,$nama_satuan)
	{
		$sql = "update master_satuan set nama_satuan=? where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_satuan, $id));
        return $queryRec;
    }
    
    public function save_satuan($nama_satuan)
	{
		$sql = "INSERT INTO `master_satuan`(`id`,`nama_satuan`)  values (null,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_satuan));
        return $queryRec;
	}
    
    public function hapus_satuan($id)
    {
        $sql = "delete from master_satuan where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        return $queryRec;
    }

    // 

    public function list_kategori ()
	{
		$sql = "select * from master_jenis";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
    
    public function get_kategori($id)
	{
		$sql = "select * from master_jenis where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $query = $this->db->query($sql, array($id));
		if ($query->num_rows() == 1) {
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return FALSE;
		}
    }
    
    public function save_edit_kategori($id,$nama_jenis)
	{
		$sql = "update master_jenis set nama_jenis=? where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_jenis, $id));
        return $queryRec;
    }
    
    public function save_kategori($nama_jenis)
	{
		$sql = "INSERT INTO `master_jenis`(`id`,`nama_jenis`)  values (null,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_jenis));
        return $queryRec;
	}
    
    public function hapus_kategori($id)
    {
        $sql = "delete from master_jenis where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        return $queryRec;
    }

    // 

    public function list_daerah ()
	{
		$sql = "select * from master_daerah_user";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
    
    public function get_daerah($id)
	{
		$sql = "select * from master_daerah_user where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $query = $this->db->query($sql, array($id));
		if ($query->num_rows() == 1) {
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return FALSE;
		}
    }
    
    public function save_edit_daerah($id,$nama_daerah)
	{
		$sql = "update master_daerah_user set nama_daerah=? where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_daerah, $id));
        return $queryRec;
    }
    
    public function save_daerah($nama_daerah)
	{
		$sql = "INSERT INTO `master_daerah_user`(`id`,`nama_daerah`)  values (null,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_daerah));
        return $queryRec;
	}
    
    public function hapus_daerah($id)
    {
        $sql = "delete from master_daerah_user where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        return $queryRec;
    }

    // 

    public function list_hutang_lama ()
	{
		$sql = "select a.id, b.nama_toko, a.jumlah_hutang from hutang_persisten a left join master_toko b on a.id_toko = b.id";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
    
    public function get_hutang_lama($id)
	{
		$sql = "select * from hutang_persisten where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $query = $this->db->query($sql, array($id));
		if ($query->num_rows() == 1) {
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return FALSE;
		}
    }
    
    public function save_edit_hutang_lama($id,$jumlah_hutang)
	{
		$sql = "update hutang_persisten set jumlah_hutang=? where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($jumlah_hutang, $id));
        return $queryRec;
    }
    
    public function save_hutang_lama($id_toko, $jumlah_hutang)
	{
		$sql = "INSERT INTO `hutang_persisten`(`id`,`id_toko`,`jumlah_hutang`, `tanggal`)  values (null,?,?,NOW())";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id_toko, $jumlah_hutang));
        return $queryRec;
	}
    
    public function hapus_hutang_lama($id)
    {
        $sql = "delete from hutang_persisten where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        return $queryRec;
    }

    public function get_toko()
	{
		$sql = "select * from master_toko";
		$queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
    }
}