<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_supplier extends CI_Model {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function list_supplier ()
	{
		$sql = "select * from master_supplier";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
	}

	public function get_daerah()
	{
		$sql = "select * from master_daerah";
		$queryRec = $this->db->query($sql)->result_array();
        return $queryRec;
	}

	public function save_supplier($nama_supplier,$telepon_supplier)
	{
		$sql = "INSERT INTO `master_supplier`(`id`,`nama_supplier`, `telepon`)  values (null,?,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_supplier, $telepon_supplier));
        return $queryRec;
	}

	public function hapus_supplier($id)
    {
        $sql = "delete from master_supplier where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        return $queryRec;
	}

	public function get_supplier($id)
	{
		$sql = "select * from master_supplier where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $query = $this->db->query($sql, array($id));
		if ($query->num_rows() == 1) {
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return FALSE;
		}
	}

	public function save_edit_supplier($id,$nama_supplier,$telepon)
	{
		$sql = "update master_supplier set nama_supplier=?, telepon=? where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_supplier, $telepon, $id));
        return $queryRec;
	}

	public function save_daerah($nama_daerah)
	{
		$sql = "INSERT INTO `master_daerah`(`id`,`nama_daerah`)  values (null,?)";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_daerah));
        return $queryRec;
	}

	public function hapus_daerah($id)
    {
        $sql = "delete from master_daerah where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($id));
        return $queryRec;
	}
	
	public function get_daerah_edit($id){
		$sql = "select * from master_daerah where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $query = $this->db->query($sql, array($id));
		if ($query->num_rows() == 1) {
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return FALSE;
		}
	}

	public function save_edit_daerah($id,$nama_daerah)
	{
		$sql = "update master_daerah set nama_daerah=? where id = ?";
        // $queryRec = $this->db->query($sql,array($tanggal,$jam,$daerah,$daerah));
        $queryRec = $this->db->query($sql, array($nama_daerah, $id));
        return $queryRec;
	}
	
}