<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
    parent::__construct();
    $this->load->model('m_toko');
	}
	
  public function index()
	{

  }
    
  public function daftar()
  {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/daftar_toko';
      $varcontent['segment'] = 'daftar_toko';
      $varcontent['root_menu'] = 'toko';
			$data_toko=$this->m_toko->list_toko();
      $varcontent['tokos'] = $data_toko;
      $data_daerah = $this->m_toko->get_daerah();
      $varcontent['daerahs'] = $data_daerah;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
	}
	
	public function tambah()
  {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/tambah_toko';
      $varcontent['segment'] = 'daftar_toko';
      $varcontent['root_menu'] = 'toko';
      
      $data_daerah = $this->m_toko->get_daerah();
      $varcontent['daerahs'] = $data_daerah;

      $data_daerah_user = $this->m_toko->get_daerah_user();
      $varcontent['daerah_user'] = $data_daerah_user;

      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function simpan_toko()
  {
    $nama_toko=$this->input->post('nama_toko');
    $daerah=$this->input->post('daerah');
    $daerah_user=$this->input->post('daerah_user');

    $data_toko = $this->m_toko->save_toko($nama_toko,$daerah,$daerah_user);
    redirect(base_url().'toko/daftar');
  }

  public function hapus_toko()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_toko->hapus_toko($id);
    redirect(base_url().'toko/daftar');
  }

  public function edit_toko()
  {
    $id = $this->uri->segment(3);


    $data_login = $this->m_toko->get_toko($id);

    $varcontent['nama_toko'] = $data_login['nama_toko'];
    $varcontent['daerah'] = $data_login['daerah'];
    $varcontent['daerah_userx'] = $data_login['daerah_user'];

    $data_daerah = $this->m_toko->get_daerah();
    $varcontent['daerahs'] = $data_daerah;

    $data_daerah_user = $this->m_toko->get_daerah_user();
    $varcontent['daerah_user'] = $data_daerah_user;

    $varcontent['pages'] = 'pages/edit_toko';
    $varcontent['root_menu'] = 'toko';
    $varcontent['segment'] = 'daftar_toko';
    $varcontent['id_toko'] = $id;
    $this->load->view('admin/overview', $varcontent);
    
  }

  public function simpan_edit_toko()
  {
    $id=$this->input->post('id');
    $nama_toko=$this->input->post('nama_toko');
    $daerah=$this->input->post('daerah');
    $daerah_user=$this->input->post('daerah_user');

    $data_satuan = $this->m_toko->save_edit_toko($id,$nama_toko,$daerah, $daerah_user);
    redirect(base_url().'toko/daftar');
  }

  public function tambah_daerah()
  {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/tambah_daerah';
      $varcontent['segment'] = 'daftar_toko';
      $varcontent['root_menu'] = 'toko';

      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function simpan_daerah()
  {
    $nama_daerah=$this->input->post('nama_daerah');

    $data_toko = $this->m_toko->save_daerah($nama_daerah);
    redirect(base_url().'toko/daftar');
  }

  public function hapus_daerah()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_toko->hapus_daerah($id);
    redirect(base_url().'toko/daftar');
  }

  public function edit_daerah()
  {
    $id = $this->uri->segment(3);


    $data_login = $this->m_toko->get_daerah_edit($id);

    $varcontent['nama_daerah'] = $data_login['nama_daerah'];

    $varcontent['pages'] = 'pages/edit_daerah';
    $varcontent['root_menu'] = 'toko';
    $varcontent['segment'] = 'daftar_toko';
    $varcontent['id_daerah'] = $id;
    $this->load->view('admin/overview', $varcontent);
    
  }

  public function simpan_edit_daerah()
  {
    $id=$this->input->post('id');
    $nama_daerah=$this->input->post('nama_daerah');

    $data_satuan = $this->m_toko->save_edit_daerah($id,$nama_daerah);
    redirect(base_url().'toko/daftar');
  }
	
}
