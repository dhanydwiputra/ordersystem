<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
    parent::__construct();
    $this->load->model('m_supplier');
	}
	
  public function index()
	{

  }
    
  public function daftar_supplier()
  {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/daftar_supplier';
      $varcontent['segment'] = 'list_supplier';
      $varcontent['root_menu'] = 'hutang';
      $suplier=$this->m_supplier->list_supplier();
      $varcontent['suppliers'] = $suplier;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
	}
	
	public function tambah()
  {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/tambah_supplier';
      $varcontent['segment'] = 'list_supplier';
      $varcontent['root_menu'] = 'hutang';

      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function simpan()
  {
    $nama_supplier=$this->input->post('nama_supplier');
    $telepon_supplier=$this->input->post('telepon_supplier');

    $data_supploer = $this->m_supplier->save_supplier($nama_supplier,$telepon_supplier);
    redirect(base_url().'supplier/daftar_supplier');
  }

  public function hapus()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_supplier->hapus_supplier($id);
    redirect(base_url().'supplier/daftar_supplier');
  }

  public function edit()
  {
    $id = $this->uri->segment(3);


    $data_login = $this->m_supplier->get_supplier($id);

    $varcontent['id'] = $data_login['id'];
    $varcontent['nama_supplier'] = $data_login['nama_supplier'];
    $varcontent['telepon_supplier'] = $data_login['telepon'];

    $varcontent['pages'] = 'pages/edit_supplier';
    $varcontent['segment'] = 'list_supplier';
    $varcontent['root_menu'] = 'hutang';
    $varcontent['id_toko'] = $id;
    $this->load->view('admin/overview', $varcontent);

  }

  public function simpan_edit_supplier()
  {
    $id=$this->input->post('id');
    $nama_supplier=$this->input->post('nama_supplier');
    $telepon_supplier=$this->input->post('telepon_supplier');

    $data_satuan = $this->m_supplier->save_edit_supplier($id,$nama_supplier,$telepon_supplier);
    redirect(base_url().'supplier/daftar_supplier');
  }

  public function tambah_daerah()
  {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/tambah_daerah';
      $varcontent['segment'] = 'daftar_toko';
      $varcontent['root_menu'] = 'toko';

      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function simpan_daerah()
  {
    $nama_daerah=$this->input->post('nama_daerah');

    $data_toko = $this->m_toko->save_daerah($nama_daerah);
    redirect(base_url().'toko/daftar');
  }

  public function hapus_daerah()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_toko->hapus_daerah($id);
    redirect(base_url().'toko/daftar');
  }

  public function edit_daerah()
  {
    $id = $this->uri->segment(3);


    $data_login = $this->m_toko->get_daerah_edit($id);

    $varcontent['nama_daerah'] = $data_login['nama_daerah'];

    $varcontent['pages'] = 'pages/edit_daerah';
    $varcontent['root_menu'] = 'toko';
    $varcontent['segment'] = 'daftar_toko';
    $varcontent['id_daerah'] = $id;
    $this->load->view('admin/overview', $varcontent);
    
  }

  public function simpan_edit_daerah()
  {
    $id=$this->input->post('id');
    $nama_daerah=$this->input->post('nama_daerah');

    $data_satuan = $this->m_toko->save_edit_daerah($id,$nama_daerah);
    redirect(base_url().'toko/daftar');
  }
	
}
