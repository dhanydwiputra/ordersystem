<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
    parent::__construct();
    $this->load->model('m_pembelian');
	}
	
  public function index()
	{
        if($this->session->userdata('login'))
    {

      $data_supplier = $this->m_pembelian->get_supplier();
      $varcontent['suppliers'] = $data_supplier;

      $varcontent['pages'] = 'pages/pembelian';
      $varcontent['segment'] = 'pembelian';
      $varcontent['root_menu'] = 'barang';
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  function get_barang(){
    $id=$this->input->post('id');
    $data=$this->m_pembelian->get_barang($id);
    echo json_encode($data);
  }

  function get_kode(){
    $id=$this->input->post('kode');
    $data=$this->m_pembelian->get_kode($id);
    echo json_encode($data);
  }

  function add_to_cart() {
    $id=$this->input->post('id_barang');
    $kode_barang=$this->input->post('kode_barang');
    $nama_barang=$this->input->post('nama_barang');
    $jumlah=$this->input->post('jumlah');
    $harga=$this->input->post('harga');

    $this->load->library('cart');

    $data = array(
      'id'      => $id,
      'kode_barang'     => $kode_barang,
      'name'   => $nama_barang,
      'qty'    => $jumlah,
      'price' => $harga );

    $this->cart->insert($data);
    $status = ['status' =>'OK','id'=>$id,'kode'=>$kode_barang,'nama'=>$nama_barang,'jumlah'=>$jumlah,'harga'=>$harga];
    echo json_encode($status);
  }

  function get_cart_data() {
    $data_cart = $this->cart->contents();
    // echo json_encode($data_cart);
    $html  = ''; 
    foreach($data_cart as $data){
      $html .= '<tr>';
      $html .= '<td>'.$data['kode_barang'].'</td>';
      $html .= '<td>'.$data['name'].'</td>';
      $html .= '<td>'.$data['qty'].'</td>';
      $html .= '<td>'.number_format($data['price'],0,',','.').'</td>';
      $html .= '<td>'.number_format(($data['qty']*$data['price']),0,',','.').'</td>';
      $html .= '<td><button onclick="remove(`'.$data['rowid'].'`)">hapus</button></td>';
      $html .= '</tr>';
    }
   echo $html;
  }

  function get_total() {
    $gt = $this->cart->total();
    echo number_format($gt,0,',','.');
  }

  function remove($rowid='')
	{
    $rowid=$this->input->post('rowid');
    $this->cart->remove($rowid);
    $status = ['status' =>'OK'];
    echo json_encode($status);
  }
  
  function save_pembelian() {
    $no_referensi=$this->input->post('no_referensi');
    $supplier=$this->input->post('supplier');
    $tanggal=$this->input->post('tanggal');
    $gt = $this->cart->total();

    $queryRecords = $this->m_pembelian->save($no_referensi,$supplier,$tanggal,$gt);

    $cart_list = $this->cart->contents();
    foreach($cart_list as $product)
    {
        $query = $this->m_pembelian->save_detail($no_referensi, $product['kode_barang'], $product['qty'], $product['price'], $product['qty']*$product['price']);
    }

    $this->cart->destroy();
    $status = ['status' =>'OK'];
    echo json_encode($status);
  }
    
}
