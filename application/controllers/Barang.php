<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
    parent::__construct();
    $this->load->model('m_barang');
    $this->load->model('m_pembelian');
	}
	
  public function index()
	{
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/daftar_barang';
      $varcontent['segment'] = 'barang';
      $varcontent['root_menu'] = 'barang';
      $data_barang=$this->m_barang->list_barang();
      $data_barang_habis=$this->m_barang->list_barang_habis();
      $varcontent['barangs'] = $data_barang;
      $varcontent['barang_habis'] = $data_barang_habis;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }
    
  public function tambah()
  {
    $varcontent['pages'] = 'pages/tambah';
    $varcontent['root_menu'] = 'barang';
    $varcontent['segment'] = 'pembelian';

    $data_kategori = $this->m_barang->get_category();
    $varcontent['kategoris'] = $data_kategori;

    $data_satuan = $this->m_barang->get_satuan();
    $varcontent['satuans'] = $data_satuan;

    $data_supplier = $this->m_pembelian->get_supplier();
    $varcontent['suppliers'] = $data_supplier;

    $this->load->view('admin/overview', $varcontent);
  }
	
	public function get_barang()
	{
		$id=$this->input->post('id');
    $data=$this->m_barang->list_barang();
    echo json_encode($data);
  }
  
  public function hapus_barang()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_barang->hapus_barang($id);
    redirect(base_url().'barang');
  }

  public function edit_barang()
  {
    $id = $this->uri->segment(3);


    $data_login = $this->m_barang->get_product($id);

    $data_kategori = $this->m_barang->get_category();
    $varcontent['kategoris'] = $data_kategori;

    $data_satuan = $this->m_barang->get_satuan();
    $varcontent['satuans'] = $data_satuan;

    $varcontent['kode_barang'] = $data_login['kode_barang'];
    $varcontent['nama_barang'] = $data_login['nama_barang'];
    $varcontent['kategori'] = $data_login['kategori'];
    $varcontent['stok'] = $data_login['stok'];
    $varcontent['satuan'] = $data_login['satuan'];
    $varcontent['harga_modal'] = $data_login['harga_modal'];

    $varcontent['pages'] = 'pages/edit_barang';
    $varcontent['root_menu'] = 'barang';
    $varcontent['segment'] = 'barang';
    $varcontent['id_barang'] = $id;
    $this->load->view('admin/overview', $varcontent);
    
  }

  public function simpan_edit()
  {
    $id=$this->input->post('id');
    $nama_barang=$this->input->post('nama_barang');
    $kode_barang=$this->input->post('kode_barang');
    $kategori=$this->input->post('kategori');
    $stok=$this->input->post('stok');
    $satuan=$this->input->post('satuan');
    $harga_modal=$this->input->post('harga_modal');

    $data_satuan = $this->m_barang->save_edit($id,$nama_barang,$kode_barang,$kategori,$stok,$satuan,$harga_modal);
    redirect(base_url().'barang');
  }

  public  function simpan_barang()
  {
    $supplier=$this->input->post('supplier');
    $nama_barang=$this->input->post('nama_barang');
    $kode_barang=$this->input->post('kode_barang');
    $kategori=$this->input->post('kategori');
    $stok=$this->input->post('stok');
    $satuan=$this->input->post('satuan');
    $harga_modal=$this->input->post('harga_modal');

    $data_satuan = $this->m_barang->save_barang($supplier,$nama_barang,$kode_barang,$kategori,$stok,$satuan,$harga_modal);
    redirect(base_url().'pembelian');
  }
}
