<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
    parent::__construct();
    $this->load->model('m_piutang');
    $this->load->model('m_hutang');
    $this->load->model('m_pesanan');
	}
	
  public function index()
	{
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/daftar_piutang';
      $varcontent['segment'] = 'daftar_piutang';
      $varcontent['root_menu'] = 'piutang';

      if($this->session->userdata['login']['role'] == 2){
        $data_piutang=$this->m_piutang->list_piutang_sales($this->session->userdata['login']['daerah']);
        $varcontent['piutangs'] = $data_piutang;
      }else {
        $data_piutang=$this->m_piutang->list_piutang();
        $varcontent['piutangs'] = $data_piutang;
      }

      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function detail()
	{
    if($this->session->userdata('login'))
    {
      $id = $this->uri->segment(3);
      $varcontent['pages'] = 'pages/detail_piutang';
      $varcontent['segment'] = 'daftar_piutang';
      $varcontent['root_menu'] = 'piutang';
      $data_piutang=$this->m_piutang->detail_piutang($id);
      $varcontent['piutangs'] = $data_piutang;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function bayar()
  {
    if($this->session->userdata('login'))
    {
      date_default_timezone_set('Asia/Jakarta');
      $date = date('Ymd');
      $date2 = date('Y-m-d');

      $kode_pembayaran_max = $this->m_piutang->get_kode_pembayaran($date2);
    //   $varcontent['kode_pesanan'] = $kode_barang_max;

      if($kode_pembayaran_max == 0) {
        $varcontent['kode_pembayaran'] = $date.'001';
      }else{
        $noUrut = (int) substr($kode_pembayaran_max, 8, 3);
        $noUrut++;
        $char = $date;
        $kodeBarang = $char . sprintf("%03s", $noUrut);
        $varcontent['kode_pembayaran'] = $kodeBarang;
      }

      $userid = $this->session->userdata('login');
      $data_toko = $this->m_pesanan->get_toko($userid['daerah']);
      $varcontent['tokos'] = $data_toko;

      $varcontent['pages'] = 'pages/bayar_piutang';
      $varcontent['segment'] = 'bayar_piutang';
      $varcontent['root_menu'] = 'piutang';
      $list_toko=$this->m_piutang->list_toko();

      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function simpan_pembayaran()
  {
    $kode_pembayaran=$this->input->post('kode_pembayaran');
    $toko=$this->input->post('toko');
    $tanggal=$this->input->post('tanggal');
    $jumlah_bayar=$this->input->post('jumlah_bayar');
    $userid = $this->session->userdata('login');

    $data_satuan = $this->m_piutang->save_pembayaran($kode_pembayaran,$toko,$tanggal,$jumlah_bayar,$userid['userid']);

    echo anchor('https://pakainfo.com/whatever', 'title="Pakainfo Jaydeep"', array('target' => '_blank', 'class' => 'new_window'));
    redirect(base_url().'piutang');
  }

  public function print_piutang()
	{
    if($this->session->userdata('login'))
    {
      $id = $this->uri->segment(3);
      $nama_toko = $this->uri->segment(4);
      $jumlah = $this->uri->segment(5);

      $data_piutang=$this->m_piutang->detail_piutang_limit($id);
      $data_header=$this->m_piutang->header_piutang($nama_toko);
      $varcontent['piutangs'] = $data_piutang;
      $varcontent['nama_toko'] = $nama_toko;
      $varcontent['jumlah'] = $jumlah;
      $varcontent['daerah'] = $data_header;
      $this->load->view('pages/print_piutang', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function hapus_pembayaran()
  {
    $id = $this->uri->segment(3);
    $data_pesanan=$this->m_piutang->hapus_pembayaran($id);
    redirect(base_url().'piutang');
  }
    
}
