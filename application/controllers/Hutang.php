<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hutang extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
    parent::__construct();
    $this->load->model('m_barang');
    $this->load->model('m_hutang');
	}
  
  public function index()
	{
    echo 'ok';
  }

  public function show()
	{
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/show_hutang';
      $varcontent['segment'] = 'show_hutang';
      $varcontent['root_menu'] = 'hutang';
      $data_hutang=$this->m_hutang->list_hutang();
      $total_hutang=$this->m_hutang->total_hutang();

      $hutang_lunas=$this->m_hutang->list_hutang_lunas();
      $varcontent['hutang_lunas'] = $hutang_lunas;

      $varcontent['hutangs'] = $data_hutang;
      $varcontent['total_hutang'] = $total_hutang;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }
    
  public function detail()
  {
    if($this->session->userdata('login'))
    {

      $raw_id = str_replace('-', '/', $this->uri->segment(3));
      $id = $raw_id;
      $cond = $this->uri->segment(4);

      if ($cond) {
        $varcontent['condition'] = true;
      }else{
        $varcontent['condition'] = false;
      }

      $varcontent['pages'] = 'pages/detail_hutang';
      $varcontent['segment'] = 'show_hutang';
      $varcontent['root_menu'] = 'hutang';
      $detail_pembelian=$this->m_hutang->list_pembelian($id);
      $header_hutang=$this->m_hutang->get_hutang($id);

      $varcontent['pembelians'] = $detail_pembelian;
      $varcontent['header'] = $header_hutang;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function detail_pembayaran()
  {
    if($this->session->userdata('login'))
    {

      $id = $this->uri->segment(3);

      $varcontent['pages'] = 'pages/detail_pembayaran';
      $varcontent['segment'] = 'show_hutang';
      $varcontent['root_menu'] = 'hutang';
      $detail_pembelian=$this->m_hutang->list_pembayaran($id);
      $header_hutang=$this->m_hutang->get_hutang($id);

      $varcontent['pembayarans'] = $detail_pembelian;
      $varcontent['header'] = $header_hutang;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function bayar()
  {
    if($this->session->userdata('login'))
    {

      $id = $this->uri->segment(3);

      $varcontent['pages'] = 'pages/bayar_supplier';
      $varcontent['segment'] = 'show_hutang';
      $varcontent['root_menu'] = 'hutang';
      $detail_pembelian=$this->m_hutang->list_pembelian($id);
      $header_hutang=$this->m_hutang->get_hutang($id);


      $varcontent['pembelians'] = $detail_pembelian;
      $varcontent['header'] = $header_hutang;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public  function simpan_pembayaran()
  {
    $nomor_referensi=$this->input->post('nomor_referensi');
    $nama_supplier=$this->input->post('nama_supplier');
    $tanggal=$this->input->post('tanggal');
    $total_pembayaran=$this->input->post('total_pembayaran');
    $jumlah_bayar=$this->input->post('jumlah_bayar');
    $userid = $this->session->userdata('login');

    $data_satuan = $this->m_hutang->save_pembayaran($nomor_referensi,$tanggal,$jumlah_bayar,$userid['userid']);
    redirect(base_url().'hutang/show');
  }
	
	public function get_barang()
	{
		$id=$this->input->post('id');
    $data=$this->m_barang->list_barang();
    echo json_encode($data);
  }
  
  public function hapus_pembelian()
  {
    $raw_id = str_replace('-', '/', $this->uri->segment(3));
    $id = $raw_id;
    $data=$this->m_hutang->hapus_pembelian($id);
    redirect(base_url().'hutang/show');
  }

  public function batal_pelunasan()
  {
    $raw_id = str_replace('-', '/', $this->uri->segment(3));
    $id = $raw_id;
    $data=$this->m_hutang->batal_pelunasan($id);
    redirect(base_url().'hutang/show');
  }

  public function edit_barang()
  {
    $id = $this->uri->segment(3);


    $data_login = $this->m_barang->get_product($id);

    $data_kategori = $this->m_barang->get_category();
    $varcontent['kategoris'] = $data_kategori;

    $data_satuan = $this->m_barang->get_satuan();
    $varcontent['satuans'] = $data_satuan;

    $varcontent['kode_barang'] = $data_login['kode_barang'];
    $varcontent['nama_barang'] = $data_login['nama_barang'];
    $varcontent['kategori'] = $data_login['kategori'];
    $varcontent['stok'] = $data_login['stok'];
    $varcontent['satuan'] = $data_login['satuan'];
    $varcontent['harga_modal'] = $data_login['harga_modal'];

    $varcontent['pages'] = 'pages/edit_barang';
    $varcontent['root_menu'] = 'barang';
    $varcontent['segment'] = 'barang';
    $varcontent['id_barang'] = $id;
    $this->load->view('admin/overview', $varcontent);
    
  }

  public function simpan_edit()
  {
    $id=$this->input->post('id');
    $nama_barang=$this->input->post('nama_barang');
    $kode_barang=$this->input->post('kode_barang');
    $kategori=$this->input->post('kategori');
    $stok=$this->input->post('stok');
    $satuan=$this->input->post('satuan');
    $harga_modal=$this->input->post('harga_modal');

    $data_satuan = $this->m_barang->save_edit($id,$nama_barang,$kode_barang,$kategori,$stok,$satuan,$harga_modal);
    redirect(base_url().'barang');
  }

  public  function simpan_barang()
  {
    $supplier=$this->input->post('supplier');
    $nama_barang=$this->input->post('nama_barang');
    $kode_barang=$this->input->post('kode_barang');
    $kategori=$this->input->post('kategori');
    $stok=$this->input->post('stok');
    $satuan=$this->input->post('satuan');
    $harga_modal=$this->input->post('harga_modal');

    $data_satuan = $this->m_barang->save_barang($supplier,$nama_barang,$kode_barang,$kategori,$stok,$satuan,$harga_modal);
    redirect(base_url().'pembelian');
  }
}
