<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
    parent::__construct();
    $this->load->model('m_pesanan');
    $this->load->model('m_pembelian');
	}
	
  public function index()
	{
    if($this->session->userdata('login'))
    {
      $userdaerah = $this->session->userdata('login');
      $data_toko = $this->m_pesanan->get_toko($userdaerah['daerah']);
      $varcontent['tokos'] = $data_toko;

      $data_supplier = $this->m_pembelian->get_supplier();
      $varcontent['suppliers'] = $data_supplier;

      date_default_timezone_set('Asia/Jakarta');
      $date = date('Ymd');
      $date2 = date('Y-m-d');

      // $kode_barang_max = $this->m_pesanan->get_kode_pesanan($date2);
    //   $varcontent['kode_pesanan'] = $kode_barang_max;
      
      // if($kode_barang_max == 0) {
      //   $varcontent['kode_pesanan'] = $date.'001';
      // }else{
      //   $noUrut = (int) substr($kode_barang_max, 8, 3);
      //   $noUrut++;
      //   $char = $date;
      //   $kodeBarang = $char . sprintf("%03s", $noUrut);
      //   $varcontent['kode_pesanan'] = $kodeBarang;
      // }

      $varcontent['pages'] = 'pages/pesanan';
      $varcontent['segment'] = 'add_pesanan';
      $varcontent['root_menu'] = 'pesanan';
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  function get_barang(){
    $id=$this->input->post('id');
    $data=$this->m_pembelian->get_barang($id);
    echo json_encode($data);
  }

  function get_kode(){
    $id=$this->input->post('kode');
    $data=$this->m_pembelian->get_kode($id);
    echo json_encode($data);
  }

  function add_to_cart() {
    $id=$this->input->post('id_barang');
    $kode_barang=$this->input->post('kode_barang');
    $nama_barang=$this->input->post('nama_barang');
    $jumlah=$this->input->post('jumlah');
    $harga=$this->input->post('harga');

    $this->load->library('cart');

    $data = array(
      'id'      => $id,
      'kode_barang'     => $kode_barang,
      'name'   => $nama_barang,
      'qty'    => $jumlah,
      'price' => $harga );

    $this->cart->insert($data);
    $status = ['status' =>'OK','id'=>$id,'kode'=>$kode_barang,'nama'=>$nama_barang,'jumlah'=>$jumlah,'harga'=>$harga];
    echo json_encode($status);
  }

  function get_cart_data() {
    $data_cart = $this->cart->contents();
    // echo json_encode($data_cart);
    $html  = ''; 
    foreach($data_cart as $data){
      $html .= '<tr>';
      $html .= '<td>'.$data['kode_barang'].'</td>';
      $html .= '<td>'.$data['name'].'</td>';
      $html .= '<td>'.$data['qty'].'</td>';
      $html .= '<td>'.number_format($data['price'],0,',','.').'</td>';
      $html .= '<td>'.number_format(($data['qty']*$data['price']),0,',','.').'</td>';
      $html .= '<td><button onclick="remove(`'.$data['rowid'].'`)">hapus</button></td>';
      $html .= '</tr>';
    }
   echo $html;
  }

  function get_total() {
    $gt = $this->cart->total();
    echo number_format($gt,0,',','.');
  }

  function remove($rowid='')
	{
    $rowid=$this->input->post('rowid');
    $this->cart->remove($rowid);
    $status = ['status' =>'OK'];
    echo json_encode($status);
  }
  
  function save_pesanan() {
    // $kode_pesanan=$this->input->post('kode_pesanan');

    $toko=$this->input->post('toko');
    $tanggal=$this->input->post('tanggal');
    $gt = $this->cart->total();
    $userid = $this->session->userdata('login');
    $user = $userid['userid'];

    $cast_tanggal = substr($tanggal,0,7).'-%';
    $kode_barang_max = $this->m_pesanan->get_kode_pesanan($cast_tanggal);

    if($kode_barang_max == 0) {
      $kode_pesanan = 'SBP-'.substr($tanggal,0,4).'.'.substr($tanggal,5,2).'.'.sprintf("%04s", 1);
    }else{
      // $noUrut = $kode_barang_max;
      // $noUrut++;
      // $kode_pesanan = 'SBP-'.substr($tanggal,0,4).'.'.substr($tanggal,5,2).'.'.sprintf("%04s", $noUrut);
      
      $kode_pesanan_max = $this->m_pesanan->get_kode_pesanan_max($cast_tanggal);
      $noUrut = (int) substr($kode_pesanan_max, 12, 5);
      $noUrut++;
      $kode_pesanan = 'SBP-'.substr($tanggal,0,4).'.'.substr($tanggal,5,2).'.'.sprintf("%04s", $noUrut);
    }

    $queryRecords = $this->m_pesanan->save_pesanan($kode_pesanan,$toko,$tanggal,$gt,$user);

    $cart_list = $this->cart->contents();
    foreach($cart_list as $product)
    {
        $query = $this->m_pesanan->save_detail($kode_pesanan, $product['kode_barang'], $product['qty'], $product['price'], $product['qty']*$product['price']);
    }

    $this->cart->destroy();
    $status = ['status' =>'OK'];
    echo json_encode($status);
  }

  function show_pesanan() {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/show_pesanan';
      $varcontent['segment'] = 'show_pesanan';
      $varcontent['root_menu'] = 'pesanan';

      if($this->session->userdata['login']['role'] == 2){
        $data_pesanan=$this->m_pesanan->show_pesanan_sales($this->session->userdata['login']['daerah']);
        $varcontent['pesanans'] = $data_pesanan;

        $data_pesanan_done=$this->m_pesanan->show_pesanan_done_sales($this->session->userdata['login']['daerah']);
        $varcontent['pesanan_dones'] = $data_pesanan_done;
      }else{
        $data_pesanan=$this->m_pesanan->show_pesanan();
        $varcontent['pesanans'] = $data_pesanan;

        $data_pesanan_done=$this->m_pesanan->show_pesanan_done();
        $varcontent['pesanan_dones'] = $data_pesanan_done;
      }
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  function detail() {
    if($this->session->userdata('login'))
    {

      $id = $this->uri->segment(3);

      $varcontent['pages'] = 'pages/detail_pesanan';
      $varcontent['segment'] = 'show_pesanan';
      $varcontent['root_menu'] = 'pesanan';
      $detail_pesanan=$this->m_pesanan->list_pesanan($id);
      $header_pesanan=$this->m_pesanan->get_pesanan($id);

      $varcontent['pesanans'] = $detail_pesanan;
      $varcontent['header'] = $header_pesanan;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  function print_pesanan() {
    if($this->session->userdata('login'))
    {

      $id = $this->uri->segment(3);

      $varcontent['pages'] = 'pages/print_pesanan';
      $varcontent['segment'] = 'show_pesanan';
      $varcontent['root_menu'] = 'pesanan';
      $detail_pesanan=$this->m_pesanan->list_pesanan($id);
      $header_pesanan=$this->m_pesanan->get_pesanan($id);

      $varcontent['pesanans'] = $detail_pesanan;
      $varcontent['header'] = $header_pesanan;
      $this->load->view('pages/print_pesanan', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function hapus()
  {
    $id = $this->uri->segment(3);
    $data_pesanan=$this->m_pesanan->get_pesanan_hapus($id);
    foreach($data_pesanan as $product)
    {
        $query = $this->m_pesanan->back_stock($product['kode_barang'], $product['qty']);
    }
    $data=$this->m_pesanan->hapus_pesanan($id);
    redirect(base_url().'pesanan/show_pesanan');
  }

  public function selesai()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_pesanan->selesaikan_pesanan($id);
    redirect(base_url().'pesanan/show_pesanan');
  }

  public function batal_selesai()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_pesanan->batal_selesai($id);
    redirect(base_url().'pesanan/show_pesanan');
  }

  public function batal_do()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_pesanan->batal_do($id);
    redirect(base_url().'pesanan/show_pesanan');
  }

  public function hapus_detail_pesanan()
  {
    $id = $this->uri->segment(3);
    $kode = $this->uri->segment(4);
    $total = $this->uri->segment(5);
    $qty = $this->uri->segment(6);
    $data_pesanan=$this->m_pesanan->hapus_detail_pesanan($kode,$id, $total, $qty);
    redirect(base_url().'pesanan/show_pesanan');
  }


}
