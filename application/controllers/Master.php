<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
  function __construct(){
    parent::__construct();
    $this->load->model('m_master');
	}

	public function user()
  {
      if($this->session->userdata('login'))
      {
        $varcontent['pages'] = 'pages/daftar_user';
        $varcontent['segment'] = 'daftar_user';
        $varcontent['root_menu'] = 'master';
        $data_user=$this->m_master->list_user();
        $varcontent['users'] = $data_user;
        $this->load->view('admin/overview', $varcontent);
      }else {
        redirect(base_url().'login');
      }
  }
  
  public function tambah_user()
  {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/tambah_user';
      $varcontent['segment'] = 'daftar_user';
      $varcontent['root_menu'] = 'master';
      
      $data_daerah = $this->m_master->get_daerah_user();
      $varcontent['daerahs'] = $data_daerah;

      $data_role = $this->m_master->get_role();
      $varcontent['roles'] = $data_role;

      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function simpan_user()
  {
    $username=$this->input->post('username');
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $daerah=$this->input->post('daerah');
    $role=$this->input->post('role');

    $data_toko = $this->m_master->save_user($username,$email, $password, $daerah, $role);
    redirect(base_url().'master/user');
  }

  public function hapus_user()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_master->hapus_user($id);
    redirect(base_url().'master/user');
  }

  public function edit_user()
  {
    $id = $this->uri->segment(3);


    $data_login = $this->m_master->get_user($id);

    $varcontent['username'] = $data_login['username'];
    $varcontent['email'] = $data_login['email'];
    $varcontent['daerah'] = $data_login['daerah'];
    $varcontent['role'] = $data_login['role'];

    
    $data_daerah = $this->m_master->get_daerah_user();
    $varcontent['daerahs'] = $data_daerah;

    $data_role = $this->m_master->get_role();
    $varcontent['roles'] = $data_role;

    $varcontent['pages'] = 'pages/edit_user';
    $varcontent['root_menu'] = 'master';
    $varcontent['segment'] = 'daftar_user';
    $varcontent['id_toko'] = $id;
    $this->load->view('admin/overview', $varcontent);
    
  }

  public function simpan_edit_user()
  {
    $id=$this->input->post('id');
    $username=$this->input->post('username');
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $daerah=$this->input->post('daerah');
    $role=$this->input->post('role');

    $data_satuan = $this->m_master->save_edit_user($id,$username,$email, $password, $daerah, $role);
    redirect(base_url().'master/user');
  }

  // 

  public function satuan()
  {
      if($this->session->userdata('login'))
      {
        $varcontent['pages'] = 'pages/daftar_satuan';
        $varcontent['segment'] = 'satuan';
        $varcontent['root_menu'] = 'master';
        $data_user=$this->m_master->list_satuan();
        $varcontent['users'] = $data_user;
        $this->load->view('admin/overview', $varcontent);
      }else {
        redirect(base_url().'login');
      }
  }

  public function tambah_satuan()
  {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/tambah_satuan';
      $varcontent['segment'] = 'satuan';
      $varcontent['root_menu'] = 'master';

      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function simpan_satuan()
  {
    $nama_satuan=$this->input->post('nama_satuan');

    $data_toko = $this->m_master->save_satuan($nama_satuan);
    redirect(base_url().'master/satuan');
  }

  public function hapus_satuan()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_master->hapus_satuan($id);
    redirect(base_url().'master/satuan');
  }

  public function edit_satuan()
  {
    $id = $this->uri->segment(3);


    $data_login = $this->m_master->get_satuan($id);

    $varcontent['nama_satuan'] = $data_login['nama_satuan'];

    $varcontent['pages'] = 'pages/edit_satuan';
    $varcontent['root_menu'] = 'master';
    $varcontent['segment'] = 'daftar_satuan';
    $varcontent['id_toko'] = $id;
    $this->load->view('admin/overview', $varcontent);
    
  }

  public function simpan_edit_satuan()
  {
    $id=$this->input->post('id');
    $nama_satuan=$this->input->post('nama_satuan');

    $data_satuan = $this->m_master->save_edit_satuan($id,$nama_satuan);
    redirect(base_url().'master/satuan');
  }

  // 

  public function kategori()
  {
      if($this->session->userdata('login'))
      {
        $varcontent['pages'] = 'pages/daftar_kategori';
        $varcontent['segment'] = 'kategori';
        $varcontent['root_menu'] = 'master';
        $data_user=$this->m_master->list_kategori();
        $varcontent['users'] = $data_user;
        $this->load->view('admin/overview', $varcontent);
      }else {
        redirect(base_url().'login');
      }
  }

  public function tambah_kategori()
  {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/tambah_kategori';
      $varcontent['segment'] = 'kategori';
      $varcontent['root_menu'] = 'master';

      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function simpan_kategori()
  {
    $nama_kategori=$this->input->post('nama_kategori');

    $data_toko = $this->m_master->save_kategori($nama_kategori);
    redirect(base_url().'master/kategori');
  }

  public function hapus_kategori()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_master->hapus_kategori($id);
    redirect(base_url().'master/kategori');
  }

  public function edit_kategori()
  {
    $id = $this->uri->segment(3);


    $data_login = $this->m_master->get_kategori($id);

    $varcontent['nama_jenis'] = $data_login['nama_jenis'];

    $varcontent['pages'] = 'pages/edit_kategori';
    $varcontent['root_menu'] = 'master';
    $varcontent['segment'] = 'kategori';
    $varcontent['id_toko'] = $id;
    $this->load->view('admin/overview', $varcontent);
    
  }

  public function simpan_edit_kategori()
  {
    $id=$this->input->post('id');
    $nama_satuan=$this->input->post('nama_kategori');

    $data_satuan = $this->m_master->save_edit_kategori($id,$nama_satuan);
    redirect(base_url().'master/kategori');
  }

  // 

  public function daerah()
  {
      if($this->session->userdata('login'))
      {
        $varcontent['pages'] = 'pages/daftar_daerah_user';
        $varcontent['segment'] = 'daerah_user';
        $varcontent['root_menu'] = 'master';
        $data_user=$this->m_master->list_daerah();
        $varcontent['users'] = $data_user;
        $this->load->view('admin/overview', $varcontent);
      }else {
        redirect(base_url().'login');
      }
  }

  public function tambah_daerah()
  {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/tambah_daerah_user';
      $varcontent['segment'] = 'daerah_user';
      $varcontent['root_menu'] = 'master';

      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function simpan_daerah_user()
  {
    $nama_kategori=$this->input->post('nama_daerah');

    $data_toko = $this->m_master->save_daerah($nama_kategori);
    redirect(base_url().'master/daerah');
  }

  public function hapus_daerah()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_master->hapus_daerah($id);
    redirect(base_url().'master/daerah');
  }

  public function edit_daerah()
  {
    $id = $this->uri->segment(3);


    $data_login = $this->m_master->get_daerah($id);

    $varcontent['nama_daerah'] = $data_login['nama_daerah'];

    $varcontent['pages'] = 'pages/edit_daerah_user';
    $varcontent['root_menu'] = 'master';
    $varcontent['segment'] = 'daerah_user';
    $varcontent['id_toko'] = $id;
    $this->load->view('admin/overview', $varcontent);
    
  }

  public function simpan_edit_daerah()
  {
    $id=$this->input->post('id');
    $nama_satuan=$this->input->post('nama_daerah');

    $data_satuan = $this->m_master->save_edit_daerah($id,$nama_satuan);
    redirect(base_url().'master/daerah');
  }

  // 

  // 

  public function hutang_lama()
  {
      if($this->session->userdata('login'))
      {
        $varcontent['pages'] = 'pages/daftar_hutang_lama';
        $varcontent['segment'] = 'hutang_lama';
        $varcontent['root_menu'] = 'master';
        $data_user=$this->m_master->list_hutang_lama();
        $varcontent['users'] = $data_user;
        $this->load->view('admin/overview', $varcontent);
      }else {
        redirect(base_url().'login');
      }
  }

  public function tambah_hutang_lama()
  {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/tambah_hutang_lama';
      $varcontent['segment'] = 'hutang_lama';
      $varcontent['root_menu'] = 'master';

      $data_toko = $this->m_master->get_toko();
      $varcontent['tokos'] = $data_toko;

      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function simpan_hutang_lama()
  {
    $id_toko=$this->input->post('toko');
    $jumlah_hutang=$this->input->post('jumlah_hutang');

    $data_toko = $this->m_master->save_hutang_lama($id_toko, $jumlah_hutang);
    redirect(base_url().'master/hutang_lama');
  }

  public function hapus_hutang_lama()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_master->hapus_hutang_lama($id);
    redirect(base_url().'master/hutang_lama');
  }

  public function edit_hutang_lama()
  {
    $id = $this->uri->segment(3);


    $data_login = $this->m_master->get_hutang_lama($id);
    
    $varcontent['id'] = $id;
    $varcontent['jumlah_hutang'] = $data_login['jumlah_hutang'];

    $varcontent['pages'] = 'pages/edit_hutang_lama';
    $varcontent['root_menu'] = 'master';
    $varcontent['segment'] = 'daerah_user';
    $this->load->view('admin/overview', $varcontent);
    
  }

  public function simpan_edit_hutang_lama()
  {
    $id=$this->input->post('id');
    $jumlah_hutang=$this->input->post('jumlah_hutang');

    $data_satuan = $this->m_master->save_edit_hutang_lama($id,$jumlah_hutang);
    
    redirect(base_url().'master/hutang_lama');
  }
}
