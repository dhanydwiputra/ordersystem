<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if($this->session->userdata('login'))
		{
			redirect(base_url().'barang');
		}else {
			$this->load->view('pages/login');
		}
	}

	public function do_login()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

		$this->load->model('M_member');
		
		$data_login = $this->M_member->login($username, $password);
		
		if (!$data_login)
		{
			$this->session->set_flashdata('error', 'Email atau Password Anda Salah');
			redirect(base_url().'login');	 
		}
		else
		{
			$login_session['userid'] = $data_login['id'];
			$login_session['username'] = $data_login['username'];
			$login_session['email'] = $data_login['email'];
			$login_session['role'] = $data_login['role'];
			$login_session['daerah'] = $data_login['daerah'];
			$this->session->set_userdata('login', $login_session);
			redirect(base_url().'barang');
			
		}
	}
	
	public function logout()
	{
		$this->session->unset_userdata('login');
		redirect(base_url().'login');
	}
}
