<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
    parent::__construct();
    $this->load->model('m_pesanan');
    $this->load->model('m_pembelian');
    $this->load->model('m_delivery');
	}
	
  public function index()
  {
    if($this->session->userdata('login'))
    {
      date_default_timezone_set('Asia/Jakarta');
      $date = date('Y/m/d');
      $time = date("his");
      $date2 = date('Y-m-d');

      $kode_barang_max = $this->m_delivery->get_kode_do($date2);

      if($kode_barang_max == 0) {
        $varcontent['kode_pesanan'] = 'DO/'.$date.'/'.'00001';
        // $varcontent['kode_pesanan'] = $date.'001';
      }else{
        $noUrut = $kode_barang_max;
        $noUrut++;
        $char = 'DO/'.$date.'/';
        $kodeBarang = $char . sprintf("%05s", $noUrut);
        $varcontent['kode_pesanan'] = $kodeBarang;
      }

      $varcontent['pages'] = 'pages/show_list_do';
      $varcontent['segment'] = 'show_do';
      $varcontent['root_menu'] = 'delivery';
      $data_pesanan=$this->m_delivery->show_pesanan();
      $varcontent['pesanans'] = $data_pesanan;

      $data_pesanan_done=$this->m_pesanan->show_pesanan_done();
      $varcontent['pesanan_dones'] = $data_pesanan_done;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  function get_barang(){
    $id=$this->input->post('id');
    $data=$this->m_pembelian->get_barang($id);
    echo json_encode($data);
  }

  function get_kode(){
    $id=$this->input->post('kode');
    $data=$this->m_pembelian->get_kode($id);
    echo json_encode($data);
  }

  function add_to_cart() {
    $id=$this->input->post('id_barang');
    $kode_barang=$this->input->post('kode_barang');
    $nama_barang=$this->input->post('nama_barang');
    $jumlah=$this->input->post('jumlah');
    $harga=$this->input->post('harga');

    $this->load->library('cart');

    $data = array(
      'id'      => $id,
      'kode_barang'     => $kode_barang,
      'name'   => $nama_barang,
      'qty'    => $jumlah,
      'price' => $harga );

    $this->cart->insert($data);
    $status = ['status' =>'OK','id'=>$id,'kode'=>$kode_barang,'nama'=>$nama_barang,'jumlah'=>$jumlah,'harga'=>$harga];
    echo json_encode($status);
  }

  function get_cart_data() {
    $data_cart = $this->cart->contents();
    // echo json_encode($data_cart);
    $html  = ''; 
    foreach($data_cart as $data){
      $html .= '<tr>';
      $html .= '<td>'.$data['kode_barang'].'</td>';
      $html .= '<td>'.$data['name'].'</td>';
      $html .= '<td>'.$data['qty'].'</td>';
      $html .= '<td>'.number_format($data['price'],0,',','.').'</td>';
      $html .= '<td>'.number_format(($data['qty']*$data['price']),0,',','.').'</td>';
      $html .= '<td><button onclick="remove(`'.$data['rowid'].'`)">hapus</button></td>';
      $html .= '</tr>';
    }
   echo $html;
  }

  function get_total() {
    $gt = $this->cart->total();
    echo number_format($gt,0,',','.');
  }

  function remove($rowid='')
	{
    $rowid=$this->input->post('rowid');
    $this->cart->remove($rowid);
    $status = ['status' =>'OK'];
    echo json_encode($status);
  }
  
  function save_pesanan() {
    $kode_pesanan=$this->input->post('kode_pesanan');
    $toko=$this->input->post('toko');
    $tanggal=$this->input->post('tanggal');
    $gt = $this->cart->total();

    $queryRecords = $this->m_pesanan->save_pesanan($kode_pesanan,$toko,$tanggal,$gt);

    $cart_list = $this->cart->contents();
    foreach($cart_list as $product)
    {
        $query = $this->m_pesanan->save_detail($kode_pesanan, $product['kode_barang'], $product['qty'], $product['price'], $product['qty']*$product['price']);
    }

    $this->cart->destroy();
    $status = ['status' =>'OK'];
    echo json_encode($status);
  }

  function show_pesanan() {
    if($this->session->userdata('login'))
    {
      $varcontent['pages'] = 'pages/show_pesanan';
      $varcontent['segment'] = 'show_pesanan';
      $varcontent['root_menu'] = 'pesanan';
      $data_pesanan=$this->m_pesanan->show_pesanan();
      $varcontent['pesanans'] = $data_pesanan;

      $data_pesanan_done=$this->m_pesanan->show_pesanan_done();
      $varcontent['pesanan_dones'] = $data_pesanan_done;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  function detail() {
    if($this->session->userdata('login'))
    {

      $id = $this->uri->segment(3);

      $varcontent['pages'] = 'pages/detail_pesanan';
      $varcontent['segment'] = 'show_pesanan';
      $varcontent['root_menu'] = 'pesanan';
      $detail_pesanan=$this->m_pesanan->list_pesanan($id);
      $header_pesanan=$this->m_pesanan->get_pesanan($id);

      $varcontent['pesanans'] = $detail_pesanan;
      $varcontent['header'] = $header_pesanan;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function hapus()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_pesanan->hapus_pesanan($id);
    redirect(base_url().'pesanan/show_pesanan');
  }

  public function selesai()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_pesanan->selesaikan_pesanan($id);
    redirect(base_url().'pesanan/show_pesanan');
  }

  public function batal_selesai()
  {
    $id = $this->uri->segment(3);
    $data=$this->m_pesanan->batal_selesai($id);
    redirect(base_url().'pesanan/show_pesanan');
  }

  public function create_do()
  {
    $no_do = $this->input->post('nomor_do');
    $driver = $this->input->post('driver');
    $tanggal = $this->input->post('tanggal');
    $no_gudang = $this->input->post('no_gudang');
    $no_kendaraan = $this->input->post('no_kendaraan');

    $data_do=$this->m_delivery->save_do($no_do,$driver,$tanggal,$no_gudang,$no_kendaraan);
    
    foreach($this->input->post('pesanan') as $pesanan)
    {
      $detail_do=$this->m_delivery->save_detail_do($no_do,$pesanan);
      $ubah_status=$this->m_delivery->ubah_status_pesanan($pesanan);
    }

    echo "ok";
  }

  public function list_do()
  {
    if($this->session->userdata('login'))
    {
      date_default_timezone_set('Asia/Jakarta');
      $date = date('Y/m/d');
      $time = date("his");
      $varcontent['kode_pesanan'] = 'DO/'.$date.'/'.$time;

      $varcontent['pages'] = 'pages/list_do';
      $varcontent['segment'] = 'list_do';
      $varcontent['root_menu'] = 'delivery';
      $data_pesanan=$this->m_delivery->show_all_do();
      $varcontent['pesanans'] = $data_pesanan;

      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function detail_delivery()
  {
    if($this->session->userdata('login'))
    {

      $id_raw = $this->uri->segment(3);
      $id = str_replace('-','/', $id_raw);

      $varcontent['pages'] = 'pages/detail_do';
      $varcontent['segment'] = 'list_do';
      $varcontent['root_menu'] = 'delivery';
      $detail_pesanan=$this->m_delivery->list_delivery($id);
      $header_pesanan=$this->m_delivery->get_delivery($id);

      $varcontent['pesanans'] = $detail_pesanan;
      $varcontent['header'] = $header_pesanan;
      $this->load->view('admin/overview', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }

  public function hapus_do()
  {
    $id_raw = $this->uri->segment(3);
    $id = str_replace('-','/', $id_raw);

    $detail_pesanan=$this->m_delivery->list_delivery($id);
    foreach($detail_pesanan as $pesanan)
    {
      $ubah_status=$this->m_delivery->ubah_status_pesanan_batal($pesanan['kode_pesanan']);
    }

    $data=$this->m_delivery->hapus_do($id);

    redirect(base_url().'delivery/list_do');
  }

  public function print_delivery()
  {
    if($this->session->userdata('login'))
    {

      $id_raw = $this->uri->segment(3);
      $id = str_replace('-','/', $id_raw);

      
      $detail_pesanan=$this->m_delivery->list_delivery_order($id);
      $header_pesanan=$this->m_delivery->get_delivery($id);

      $varcontent['pesanans'] = $detail_pesanan;
      $varcontent['header'] = $header_pesanan;
      $this->load->view('pages/print_delivery', $varcontent);
    }else {
      redirect(base_url().'login');
    }
  }


}
