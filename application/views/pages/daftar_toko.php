<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Manajemen Toko-Toko
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#home" onclick="change_all()" data-toggle="tab">Daftar Toko</a></li>
                    <li role="presentation"><a href="#daerah" onclick="change_daerah()" data-toggle="tab">Daftar Daerah</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        <div style="margin-bottom: 10px;">
                            <a href="<?php echo base_url() ?>toko/tambah"class="btn bg-pink waves-effect">
                                <i class="material-icons">library_add</i>
                                <span>Tambah Toko Baru</span>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nama Toko</th>
                                        <th>Daerah</th>
                                        <th>Daerah User</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php if (!empty($tokos)) { ?>

                                    <?php foreach ($tokos as $rows) { ?>
                                        <tr>
                                            <td><?php echo $rows['id'] ?></td>
                                            <td><?php echo $rows['nama_toko'] ?></td>
                                            <td><?php echo $rows['nama_daerah'] ?></td>
                                            <td><?php echo $rows['nama_daerah_user'] ?></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>toko/hapus_toko/<?php echo $rows['id'] ?>" class="btn btn-primary waves-effect">
                                                    <i class="material-icons">delete</i>
                                                </a>
                                                <a href="<?php echo base_url() ?>toko/edit_toko/<?php echo $rows['id'] ?>" class="btn btn-success waves-effect">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                            </td>
                                        </tr>

                                    <?php } ?>
                                <?php } else { ?>

                                    <p>Tidak ada Toko</p>

                                <?php } ?>
                            </table>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade in" id="daerah">
                        <div style="margin-bottom: 10px;">
                            <a href="<?php echo base_url() ?>toko/tambah_daerah"class="btn bg-blue waves-effect">
                                <i class="material-icons">library_add</i>
                                <span>Tambah Daerah Baru</span>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nama Daerah</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php if (!empty($daerahs)) { ?>

                                    <?php foreach ($daerahs as $rows) { ?>
                                        <tr>
                                            <td><?php echo $rows['id'] ?></td>
                                            <td><?php echo $rows['nama_daerah'] ?></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>toko/hapus_daerah/<?php echo $rows['id'] ?>" class="btn btn-primary waves-effect">
                                                    <i class="material-icons">delete</i>
                                                </a>
                                                <a href="<?php echo base_url() ?>toko/edit_daerah/<?php echo $rows['id'] ?>" class="btn btn-success waves-effect">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                            </td>
                                        </tr>

                                    <?php } ?>
                                <?php } else { ?>

                                    <p>Tidak ada Daerah</p>

                                <?php } ?>
                            </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>