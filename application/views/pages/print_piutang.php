<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Piutang Pembelian</title>
    
    <style>

    @page { size: 5.5in 8.5in ;  margin: 5mm; }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 1px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    /* .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    } */
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: left;
        }
    }

    table {
        font-size:12px;
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }

    .margin-10 {
        margin-top: 30px;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="4">
                    <table>
                        <tr>
                            <td class="title" style="text-align:center;">
                                <img src="<?php echo base_url('assets/images/logo.jpg') ?>" style="width:100%; max-width:80px;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="top">
                <td colspan="4">
                    <table>
                        <tr>
                            <td style="text-align:center;font-size:12px;">
                                TUNAS BARU<br>
                                Trade & Distribution<br>
                                Batipuah Selatan, Tanah Datar, Sumatera Barat <br>
                                Nama Toko : <?php echo str_replace('%20',' ', $nama_toko); ?> <br>
                                Daerah : <?php echo $daerah; ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="4">
                    <table>
                        <tr>
                            <td>
                                <!-- Nama Toko : <?php echo $header['kode_pesanan']; ?><br>
                                Tanggal: <?php echo $header['tanggal_pesanan']; ?><br>
                                Nama Toko: <b><?php echo $header['nama_toko']; ?></b> -->
                            </td>
                            
                            <!-- <td>
                                Acme Corp.<br>
                                John Doe<br>
                                john@example.com
                            </td> -->
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Tipe
                </td>
                
                <td>
                    Tanggal
                </td>

                <td>
                    Jumlah
                </td>
            </tr>
            <?php if (!empty($piutangs)) { ?>

                <?php foreach ($piutangs as $rows) { ?>
                    <tr class="item">
                        <td>
                            <?php echo $rows['type'] ?>
                        </td>
                        
                        <td>
                            <?php echo $rows['tanggal'] ?>
                        </td>

                        <td>
                            Rp. <?php echo number_format($rows['total'],0,',','.'); ?>
                        </td>
                    </tr>

                <?php } ?>
            <?php } else { ?>

            <?php } ?>
            
            <tr class="total">
                <td></td>
                <td style="text-align: right;"><b>Sisa Hutang:</b></td>
                
                <td>
                    Rp. <?php echo number_format($jumlah,0,',','.'); ?>
                </td>
            </tr>
            
            <tr>
                <td colspan="4">
                        <table>
                            <tr>
                                <td style="text-align:center;">
                                    <hr>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tr>

            <tr>

            
                <td colspan="4">
                    <table>
                        <tr>
                            <td style="text-align:center;">
                                Terima Kasih
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <script>
        window.print();
    </script>
</body>
</html>
