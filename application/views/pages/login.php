<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
    <style>
        .login-page {
            background-color: #fff;
        }

        .logo-center-image img {
            width: 150px;
            border-radius: 50%;
        }

        .logo-center-image {
            text-align: center;
        }

        .top-10 {
            margin-top: 10px;
        }
    </style>
</head>
<body class="login-page">

    <div class="login-box">
        <!-- <div class="logo">
            <a href="javascript:void(0);" style="color: #151515;font-size: 18px;margin-top:10px;">Sistem Informasi<b> Order Barang</b></a>
        </div> -->
        <div class="card">
            <div class="body">
                <form id="sign_in" action="<?php echo base_url() ?>login/do_login" method="post">
                    <div class="logo-center-image">
                        <img src="<?php echo base_url('assets/images/logo.jpg') ?>" alt="">
                    </div>
                    <div class="msg top-10">Silahkan Login ke Akun Anda</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <?php if($this->session->flashdata('error') == TRUE){ ?>  
                        <p style="text-align: center;color:#951F2B;">Error : <?php echo $this->session->flashdata('error'); ?></p>
                    <?php } ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">LOG IN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

  <?php $this->load->view("admin/_partials/js.php") ?>
</body>