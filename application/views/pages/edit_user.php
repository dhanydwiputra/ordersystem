<!-- Vertical Layout | With Floating Label -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  Edit User
              </h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="material-icons">more_vert</i>
                      </a>
                      <ul class="dropdown-menu pull-right">
                          <li><a href="javascript:void(0);">Action</a></li>
                          <li><a href="javascript:void(0);">Another action</a></li>
                          <li><a href="javascript:void(0);">Something else here</a></li>
                      </ul>
                  </li>
              </ul>
          </div>
          <div class="body">
              <form action="<?php echo base_url() ?>master/simpan_edit_user" method="post">
                  <input type="hidden" name="id" value="<?php echo $id_toko; ?>"> 
                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="username" name="username" class="form-control" value="<?php echo $username; ?>">
                          <label class="form-label">Username</label>
                      </div>
                  </div>

                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="email" name="email" class="form-control" value="<?php echo $email; ?>">
                          <label class="form-label">email</label>
                      </div>
                  </div>

                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="password" name="password" class="form-control" value="" required>
                          <label class="form-label">password</label>
                      </div>
                  </div>

                  <div class="form-group">
                      <label for="email_address">Role</label>
                      <div class="form-line">
                        <select class="form-control show-tick" name='role'>
                            <option value="">Pilih Role User</option>
                            <?php foreach ($roles as $rows) { ?>
                                <?php $i++; ?>
                                <option value="<?php echo $rows['id_role'] ?>" <?php if ($rows['id_role']== $role) { ?>selected="selected"<?php } ?> ><?php echo ucwords($rows['nama_role']) ?></option>
                            <?php } ?>
                        </select>
                      </div>
                  </div>

                  <div class="form-group">
                      <label for="email_address">Daerah</label>
                      <div class="form-line">
                        <select class="form-control show-tick" name='daerah'>
                            <option value="">Pilih Daerah</option>
                            <?php foreach ($daerahs as $rows) { ?>
                                <?php $i++; ?>
                                <option value="<?php echo $rows['id'] ?>" <?php if ($rows['id']== $daerah) { ?>selected="selected"<?php } ?> ><?php echo ucwords($rows['nama_daerah']) ?></option>
                            <?php } ?>
                        </select>
                      </div>
                  </div>

                  <a href="<?php echo base_url() ?>master/user" class="btn bg-red btn-lg waves-effect m-t-15 m-r-10">Batal</a>
                  <button type="submit" class="btn bg-blue btn-lg waves-effect m-t-15">Simpan</button>
              </form>
          </div>
      </div>
  </div>
</div>
<!-- Vertical Layout | With Floating Label -->