<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Daftar Satuan
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#home" onclick="change_all()" data-toggle="tab">Daftar Satuan</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        <div style="margin-bottom: 10px;">
                            <a href="<?php echo base_url() ?>master/tambah_satuan"class="btn bg-pink waves-effect">
                                <i class="material-icons">library_add</i>
                                <span>Tambah Satuan Baru</span>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nama Satuan</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <?php if (!empty($users)) { ?>

                                    <?php foreach ($users as $rows) { ?>
                                        <tr>
                                            <td><?php echo $rows['id'] ?></td>
                                            <td><?php echo $rows['nama_satuan'] ?></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>master/hapus_satuan/<?php echo $rows['id'] ?>" class="btn btn-primary waves-effect">
                                                    <i class="material-icons">delete</i>
                                                </a>
                                                <a href="<?php echo base_url() ?>master/edit_satuan/<?php echo $rows['id'] ?>" class="btn btn-success waves-effect">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                            </td>
                                        </tr>

                                    <?php } ?>
                                <?php } else { ?>

                                    <p>Tidak ada Satuan</p>

                                <?php } ?>
                            </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>