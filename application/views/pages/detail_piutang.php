<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Detail Piutang
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <!-- <div style="margin-bottom: 10px;">
                    <a href="<?php echo base_url() ?>barang/tambah"class="btn bg-pink waves-effect">
                        <i class="material-icons">library_add</i>
                        <span>Tambah Barang Baru</span>
                    </a>
                </div> -->
                <!-- Nav tabs -->

                <div class="row">

                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <a href="<?php echo base_url() ?>piutang" class="btn btn-info waves-effect">
                          <<< Kembali
                      </a>
                  </div>

                </div>

                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#home" onclick="change_all()" data-toggle="tab">Semua Piutang</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>Tipe</th>
                                        <th>Tanggal</th>
                                        <th>Total</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                                <?php if (!empty($piutangs)) { ?>

                                    <?php foreach ($piutangs as $rows) { ?>
                                        <tr>
                                            <td><?php echo $rows['type'] ?></td>
                                            <td><?php echo $rows['tanggal'] ?></td>
                                            <td><?php echo number_format($rows['total'],0,',','.') ?></td>
                                            <td>
                                                <?php if ($rows['type'] == 'Pembayaran') { ?>
                                                    <a href="<?php echo base_url() ?>piutang/hapus_pembayaran/<?php echo $rows['id'] ?>" class="btn btn-success waves-effect">
                                                        <i class="material-icons"><i class="material-icons">delete</i></i>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        </tr>

                                    <?php } ?>
                                <?php } else { ?>

                                    <p>Hutang Kosong</p>

                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    function change_all () {
        document.title = 'Daftar Semua Barang';
    }

    function out_stock () {
        document.title = 'Daftar Barang dengan Stok Sedikit';
    }
</script>
<script type="text/javascript">
    $(document).ready(function(){
        // $('#kategori').change(function(){
        //     var id=$(this).val();
        //     $.ajax({
        //         url : "<?php echo base_url();?>barang/get_barang",
        //         method : "POST",
        //         data : {id: id},
        //         async : false,
        //         dataType : 'json',
        //         success: function(data){
        //             alert(data);
        //             var html = '';
        //             var i;
        //             for(i=0; i<data.length; i++){
        //                 html += '<option>'+data[i].subkategori_nama+'</option>';
        //             }
        //             $('.subkategori').html(html);
                     
        //         }
        //     });
        // });
    });

    // $.ajax({
    //     url : "<?php echo base_url();?>barang/get_barang",
    //     method : "POST",
    //     data : {id: 1},
    //     dataType : 'json',
    //     success: function(data){
    //         console.log(data);
    //         var html = '';
    //         var i;
    //         for(i=0; i<data.length; i++){
    //             html += '<tr>'+
    //                 '<td>'+data[i].kode_barang+'</td>'+
    //                 '<td>'+data[i].nama_barang+'</td>'+
    //                 '<td>'+data[i].kategori+'</td>'+
    //                 '<td>'+data[i].stok+'</td>'+
    //                 '<td>'+data[i].satuan+'</td>'+
    //                 '<td>'+data[i].harga_modal+'</td>'+
    //                 '<td>'+
    //                     '<button type="button" class="btn btn-primary waves-effect">' +
    //                         '<i class="material-icons">delete</i>' +
    //                     '</button>' + '&nbsp;' +
    //                     '<button type="button" class="btn btn-success waves-effect">' +
    //                         '<i class="material-icons">mode_edit</i>' +
    //                     '</button>' +
    //                 '</td>'+
    //             +'</tr>';
    //         }
    //         $('#table_barang').html(html);
                
    //     }
    // });
</script>