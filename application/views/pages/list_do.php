<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Daftar Delivery Order
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#home" onclick="change_all()" data-toggle="tab">Daftar Delivery Order</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>Nomor DO</th>
                                        <th>Driver</th>
                                        <th>Tanggal</th>
                                        <th>No Gudang</th>
                                        <th>No Kendaraan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php if (!empty($pesanans)) { ?>
                                    <?php foreach ($pesanans as $rows) { ?>
                                        <tr>
                                            <td><?php echo $rows['no_do'] ?></td>
                                            <td><?php echo $rows['driver'] ?></td>
                                            <td><?php echo $rows['tanggal'] ?></td>
                                            <td><?php echo $rows['no_gudang'] ?></td>
                                            <td><?php echo $rows['no_kendaraan'] ?></td>
                                            <td>
                                              <a href="<?php echo base_url() ?>delivery/detail_delivery/<?php echo str_replace('/','-',$rows['no_do']) ?>" class="btn btn-success waves-effect">
                                                  <i class="material-icons">find_in_page</i>
                                              </a>

                                              <a href="<?php echo base_url() ?>delivery/hapus_do/<?php echo str_replace('/','-',$rows['no_do']) ?>" class="btn btn-primary waves-effect">
                                                  <i class="material-icons">delete</i>
                                              </a>

                                              <a target="_blank" href="<?php echo base_url() ?>delivery/print_delivery/<?php echo str_replace('/','-',$rows['no_do']) ?>" class="btn btn-info waves-effect">
                                                  <i class="material-icons">print</i>
                                              </a>
                                            </td>
                                        </tr>

                                    <?php } ?>
                                <?php } else { ?>

                                <?php } ?>
                                </form>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
  $('.datepicker').bootstrapMaterialDatePicker({
      format: 'YYYY-MM-DD',
      clearButton: true,
      weekStart: 1,
      time: false
  });
  function create_do() {

    var nomor_do = document.getElementById('nomor_do').value;
    var driver = document.getElementById('driver').value;
    var tanggal = document.getElementById('tanggal').value;
    var no_gudang = document.getElementById('no_gudang').value;
    var no_kendaraan = document.getElementById('no_kendaraan').value;

    var pesanan = [];  
    $('.pesanan').each(function(){  
        if($(this).is(":checked"))  
        {  
          pesanan.push($(this).val());  
        }  
    });  
      $.ajax({
          url : "<?php echo base_url();?>delivery/create_do",
          method : "POST",
          data : {pesanan: pesanan,nomor_do:nomor_do,driver:driver,tanggal:tanggal,no_gudang:no_gudang,no_kendaraan:no_kendaraan},
          success: function(data){
            if (data == "OK"){
              alert('Sukses');
            }
          }
      });
		}
</script>