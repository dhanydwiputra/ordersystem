<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Daftar Pesanan
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#home" onclick="change_all()" data-toggle="tab">Daftar Pesanan</a></li>
                    <li role="presentation"><a href="#profile" onclick="out_stock()" data-toggle="tab">Pesanan Selesai</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>Kode Pesanan</th>
                                        <th>Tanggal</th>
                                        <th>Toko</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php if (!empty($pesanans)) { ?>

                                    <?php foreach ($pesanans as $rows) { ?>
                                        <tr>
                                            <td><?php echo $rows['kode_pesanan'] ?></td>
                                            <td><?php echo $rows['tanggal_pesanan'] ?></td>
                                            <td><?php echo $rows['nama_toko'] ?></td>
                                            <td><?php echo $rows['total'] ?></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>pesanan/hapus/<?php echo $rows['kode_pesanan'] ?>" class="btn btn-primary waves-effect">
                                                    <i class="material-icons">delete</i>
                                                </a>
                                                <a href="<?php echo base_url() ?>pesanan/detail/<?php echo $rows['kode_pesanan'] ?>" class="btn btn-success waves-effect">
                                                    <i class="material-icons">find_in_page</i>
                                                </a>
                                                <a target="_BLANK" href="<?php echo base_url() ?>pesanan/print_pesanan/<?php echo $rows['kode_pesanan'] ?>" class="btn btn-info waves-effect">
                                                    <i class="material-icons">print</i>
                                                </a>
                                            </td>
                                        </tr>

                                    <?php } ?>
                                <?php } else { ?>

                                    <p>Tidak ada Toko</p>

                                <?php } ?>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile">
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                      <th>Id</th>
                                      <th>Kode Pesanan</th>
                                      <th>Tanggal</th>
                                      <th>Toko</th>
                                      <th>Total</th>
                                      <th>Action</th>
                                    </tr>
                                </thead>
                                <?php if (!empty($pesanan_dones)) { ?>

                                    <?php foreach ($pesanan_dones as $rows) { ?>
                                      <tr>
                                            <td><?php echo $rows['id'] ?></td>
                                            <td><?php echo $rows['kode_pesanan'] ?></td>
                                            <td><?php echo $rows['tanggal_pesanan'] ?></td>
                                            <td><?php echo $rows['nama_toko'] ?></td>
                                            <td><?php echo $rows['total'] ?></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>pesanan/hapus/<?php echo $rows['kode_pesanan'] ?>" class="btn btn-primary waves-effect">
                                                    <i class="material-icons">delete</i>
                                                </a>
                                                <a href="<?php echo base_url() ?>pesanan/detail/<?php echo $rows['kode_pesanan'] ?>" class="btn btn-success waves-effect">
                                                    <i class="material-icons">find_in_page</i>
                                                </a>
                                            </td>
                                        </tr>

                                    <?php } ?>
                                <?php } else { ?>

                                <?php } ?>
                            </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>