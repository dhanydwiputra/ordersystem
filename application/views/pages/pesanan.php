<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="card">
            <div class="body">
              <form>
                  <label for="email_address">Kode Barang</label>
                  <div class="form-group">
                      <div class="form-line">
                          <input type="text" id="kode_barang" class="form-control" placeholder="Masukan Kode Barang">
                      </div>
                  </div>
                  <button type="button" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#largeModal">Cari Barang</button>
                  <button type="button" class="btn btn-primary waves-effect" onclick="get_kode()">Pilih >>></button>
              </form>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
        <div class="card">
            <div class="body">
              <form>
									<input type="hidden" id="id_barang" name="id_barang">
                  <label for="nama_barang">Nama Barang</label>
                  <div class="form-group">
                      <div class="form-line">
                          <input type="text" id="nama_barang" name="nama_barang" class="form-control" readonly>
                      </div>
                  </div>
                  <label for="harga_modal">Harga</label>
                  <div class="form-group">
                      <div class="form-line">
                          <input type="text" id="harga_modal" name="harga_modal" class="form-control">
                      </div>
                  </div>
									<label for="jumlah_stok">Jumlah</label>
                  <div class="form-group">
                      <div class="form-line">
                          <input type="number" id="jumlah_stok" name="jumlah_stok" class="form-control">
                      </div>
                  </div>
                  <button type="button" onclick="add_to_cart()" class="btn btn-primary waves-effect">Masukan</button>
              </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
					<div class="body">
						<form>
								<!-- <div class="col-sm-4">
									<label for="email_address">Kode Pesanan</label>
									<div class="form-group">
											<div class="form-line">
													<input type="text" value="<?php echo $kode_pesanan; ?>" id="kode_pesanan" class="form-control" readonly>
											</div>
									</div>
								</div> -->
								<div class="col-sm-4">
									<div class="form-group">
											<label for="email_address">Toko</label>
											<div class="form-line">
												<select class="form-control show-tick" id="toko" name='toko'>
														<option value="">Pilih Toko</option>
														<?php foreach ($tokos as $rows) { ?>
																<?php $i++; ?>
																<option value="<?php echo $rows['id'] ?>"><?php echo ucwords($rows['nama_toko']) ?></option>
														<?php } ?>
												</select>
											</div>
									</div>
								</div>
								<div class="col-sm-4">
										<div class="form-group">
												<label for="email_address">Tanggal</label>
												<div class="form-line">
														<input type="text" id="tanggal" class="datepicker form-control" placeholder="Please choose a date...">
												</div>
										</div>
								</div>
						</form>
						<!-- Striped Rows -->
            <div class="row m-t-15">
							<table class="table table-striped">
									<thead>
											<tr>
													<th>Kode</th>
													<th>Nama Barang</th>
													<th>Qty</th>
													<th>Satuan</th>
                          <th>Total</th>
                          <th>Action</th>
											</tr>
									</thead>
									<tbody id="cart_data">
									</tbody>
							</table>
              <div style="float:right;margin-right:10px;">
                <h4 style="display:inline-block;">Grand Total : Rp. </h4>
                <h4 style="display:inline-block;" id="grand-total"></h4>
              </div>
            </div>
            <!-- #END# Striped Rows -->
						<!-- <button type="button" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#largeModal">Print</button> -->
						<button type="button" onclick="save_pembelian()" class="btn btn-primary waves-effect" onclick="get_kode()">Simpan</button>
					</div>
			</div>
	</div>
</div>

<!-- Large Size -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Cari Barang</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label for="email_address">Supplier</label>
                  <div class="form-line">
                    <select class="form-control show-tick" id="supplier" name='supplier' onchange="get_barang(this)">
                        <option value="">Pilih Suplier</option>
                        <?php foreach ($suppliers as $rows) { ?>
                            <?php $i++; ?>
                            <option value="<?php echo $rows['id'] ?>"><?php echo ucwords($rows['nama_supplier']) ?></option>
                        <?php } ?>
                    </select>
                  </div>
              </div>
              <div style="margin-bottom: 10px;">
                  <a href="<?php echo base_url() ?>barang/tambah"class="btn bg-pink waves-effect">
                      <i class="material-icons">library_add</i>
                      <span>Tambah Barang Baru</span>
                  </a>
              </div>
              <div class="list-group" id="daftar_barang">
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
		$('.datepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        clearButton: true,
        weekStart: 1,
        time: false
    });

    refresh_table();
	
    function get_barang(selectObject){
      var id = selectObject.value; 
      $.ajax({
          url : "<?php echo base_url();?>pembelian/get_barang",
          method : "POST",
          data : {id: id},
          dataType : 'json',
          success: function(data){
              var html = '';
              var i;
              for(i=0; i<data.length; i++){
                  html += '<button onclick="add_kode(this)" data-dismiss="modal" value="'+data[i].kode_barang+'" type="button" class="list-group-item">'+data[i].kode_barang+' - '+data[i].nama_barang+' - (Stok '+data[i].stok+')</button>';
              }
              $('#daftar_barang').html(html);
                
          }
      });
    }

    function add_kode(val){
      var kode_barang = val.value; 
      document.getElementById('kode_barang').value=kode_barang;
    }

    function get_kode(){
      var kode = document.getElementById('kode_barang').value;
      $.ajax({
          url : "<?php echo base_url();?>pembelian/get_kode",
          method : "POST",
          data : {kode: kode},
          dataType : 'json',
          success: function(data){
              if(data.length == 0) {
                alert('Data Tidak ditemukan!!');
                document.getElementById('id_barang').value='';
                document.getElementById('nama_barang').value='';
                document.getElementById('harga_modal').value='';
                document.getElementById('jumlah_stok').value='';
              }else{
                document.getElementById('id_barang').value=data[0].id;
                document.getElementById('nama_barang').value=data[0].nama_barang;
                document.getElementById('harga_modal').value=data[0].harga_modal;
                document.getElementById('jumlah_stok').value='';
              } 
          }
      });
    }

		function add_to_cart() {
			var id_barang = document.getElementById('id_barang').value;
			var kode_barang = document.getElementById('kode_barang').value;
			var nama_barang = document.getElementById('nama_barang').value;
			var jumlah = document.getElementById('jumlah_stok').value;
			var harga = document.getElementById('harga_modal').value;
      $.ajax({
          url : "<?php echo base_url();?>pembelian/add_to_cart",
          method : "POST",
          data : {id_barang: id_barang, kode_barang: kode_barang, nama_barang: nama_barang, jumlah: jumlah, harga: harga},
          dataType : 'json',
          success: function(data){
              if(data.status) {
								refresh_table();
							}
          }
      });
		}

		function refresh_table() {
			$.ajax({
          url : "<?php echo base_url();?>pembelian/get_cart_data",
          method : "GET",
          dataType : 'html',
          success: function(data){
            // var html = '';
            // var i;
            // for(i=0; i<data.length; i++){
            //     html += '<tr>'+
            //             '<td>'+data[i].kode_barang+'</td>'+
            //             '<td>'+data[i].name+'</td>'+
            //             '<td>'+data[i].price+'</td>'+
            //             '</tr>';
            // }
            $('#cart_data').html(data);
            get_total();
          }
      });
		}

    function remove(rowid) {
      $.ajax({
          url : "<?php echo base_url();?>pembelian/remove",
          method : "POST",
          data : {rowid: rowid},
          success: function(data){
            refresh_table();
          }
      });
    }

    function get_total() {
      $.ajax({
          url : "<?php echo base_url();?>pembelian/get_total",
          method : "GET",
          success: function(data){
            $('#grand-total').html(data);
          }
      });
    }

    function save_pembelian() {
			// var kode_pesanan = document.getElementById('kode_pesanan').value;
			var toko = document.getElementById('toko').value;
			var tanggal = document.getElementById('tanggal').value;

      if(toko == '' || tanggal == '')
      {
          alert ('Mohon isi Kode Pesanan , supplier, dan tanggal');
      }
      else
      {
        $.ajax({
            url : "<?php echo base_url();?>pesanan/save_pesanan",
            method : "POST",
            data : {toko: toko, tanggal: tanggal},
            dataType : 'json',
            success: function(data){
                console.log(data);
                if(data.status == 'OK') {
                  alert("data telah dimasukan");
                  refresh_table();
                  document.getElementById('kode_pesanan').value = "";
                  document.getElementById('toko').value = "";
                  document.getElementById('tanggal').value = "";
                }
            }
        });
      }
		}

</script>