<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Daftar Pesanan
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#home" onclick="change_all()" data-toggle="tab">Daftar Pesanan</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        <div class="row">
                          <div class="col-sm-4">
                            <label for="email_address">Kode Pesanan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="<?php echo $kode_pesanan; ?>" id="nomor_do" class="form-control" readonly>
                                </div>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <label for="email_address">Nama Driver</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="" id="driver" class="form-control">
                                </div>
                            </div>
                          </div>
                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label for="email_address">Tanggal</label>
                                  <div class="form-line">
                                      <input type="text" id="tanggal" class="datepicker form-control" placeholder="Please choose a date...">
                                  </div>
                              </div>
                          </div>
                          <div class="col-sm-4">
                            <label for="email_address">No Gudang</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="" id="no_gudang" class="form-control">
                                </div>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <label for="email_address">No Kendaraan</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" value="" id="no_kendaraan" class="form-control">
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-basic-example">
                                <thead>
                                    <tr>
                                        <th>Pilih</th>
                                        <th>Kode Pesanan</th>
                                        <th>Tanggal</th>
                                        <th>Toko</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php if (!empty($pesanans)) { ?>
                                    <?php foreach ($pesanans as $rows) { ?>
                                        <tr>
                                            <td>
                                              <input type="checkbox" name="myCheckboxes[]" id="<?php echo $rows['kode_pesanan'] ?>" value="<?php echo $rows['kode_pesanan'] ?>" class="filled-in pesanan">
                                              <label for="<?php echo $rows['kode_pesanan'] ?>"></label>
                                            </td>
                                            <td><?php echo $rows['kode_pesanan'] ?></td>
                                            <td><?php echo $rows['tanggal_pesanan'] ?></td>
                                            <td><?php echo $rows['nama_toko'] ?></td>
                                            <td><?php echo $rows['total'] ?></td>
                                            <td>
                                              <a href="<?php echo base_url() ?>pesanan/detail/<?php echo $rows['kode_pesanan'] ?>" class="btn btn-success waves-effect">
                                                  <i class="material-icons">find_in_page</i>
                                              </a>
                                            </td>
                                        </tr>

                                    <?php } ?>
                                <?php } else { ?>

                                    <p>Tidak ada Toko</p>

                                <?php } ?>
                                </form>
                            </table>
                        </div>
                    </div>
                    <button type="button" onclick="create_do()" class="btn btn-primary waves-effect">Buat DO</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
  $('.datepicker').bootstrapMaterialDatePicker({
      format: 'YYYY-MM-DD',
      clearButton: true,
      weekStart: 1,
      time: false
  });
  function create_do() {

    var nomor_do = document.getElementById('nomor_do').value;
    var driver = document.getElementById('driver').value;
    var tanggal = document.getElementById('tanggal').value;
    var no_gudang = document.getElementById('no_gudang').value;
    var no_kendaraan = document.getElementById('no_kendaraan').value;

    var pesanan = [];  
    $('.pesanan').each(function(){  
        if($(this).is(":checked"))  
        {  
          pesanan.push($(this).val());  
        }  
    });  
      $.ajax({
          url : "<?php echo base_url();?>delivery/create_do",
          method : "POST",
          data : {pesanan: pesanan,nomor_do:nomor_do,driver:driver,tanggal:tanggal,no_gudang:no_gudang,no_kendaraan:no_kendaraan},
          success: function(data){
            alert('sukses membuat DO');
            location.reload();
          }
      });
		}
</script>