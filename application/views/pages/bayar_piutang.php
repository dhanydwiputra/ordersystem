<!-- Vertical Layout | With Floating Label -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  Bayar Piutang
              </h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="material-icons">more_vert</i>
                      </a>
                      <ul class="dropdown-menu pull-right">
                          <li><a href="javascript:void(0);">Action</a></li>
                          <li><a href="javascript:void(0);">Another action</a></li>
                          <li><a href="javascript:void(0);">Something else here</a></li>
                      </ul>
                  </li>
              </ul>
          </div>
          <div class="body">
              <form action="<?php echo base_url() ?>piutang/simpan_pembayaran" method="post">
                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="kode_pembayaran" name="kode_pembayaran" class="form-control" value="<?php echo $kode_pembayaran; ?>" readonly>
                          <label class="form-label">Kode Pembayaran</label>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="email_address">Toko</label>
                      <div class="form-line">
                        <select class="form-control show-tick" id="toko" name='toko'>
                            <option value="">Pilih Toko</option>
                            <?php foreach ($tokos as $rows) { ?>
                                <?php $i++; ?>
                                <option value="<?php echo $rows['id'] ?>"><?php echo ucwords($rows['nama_toko']) ?></option>
                            <?php } ?>
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="email_address">Tanggal Pembayaran</label>
                      <div class="form-line">
                          <input type="text" id="tanggal" name="tanggal" class="datepicker form-control" placeholder="Please choose a date..." required>
                      </div>
                  </div>

                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="jumlah_bayar" name="jumlah_bayar" class="form-control" value="" required>
                          <label class="form-label">Jumlah dibayar</label>
                      </div>
                  </div>
                  <button type="submit" class="btn bg-blue btn-lg waves-effect m-t-15">Simpan</button>
              </form>
          </div>
      </div>
  </div>
</div>
<!-- Vertical Layout | With Floating Label -->

<script>
  $('.datepicker').bootstrapMaterialDatePicker({
      format: 'YYYY-MM-DD',
      clearButton: true,
      weekStart: 1,
      time: false
  });
</script>