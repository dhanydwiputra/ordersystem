<!-- Vertical Layout | With Floating Label -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  Tambah Barang
              </h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="material-icons">more_vert</i>
                      </a>
                      <ul class="dropdown-menu pull-right">
                          <li><a href="javascript:void(0);">Action</a></li>
                          <li><a href="javascript:void(0);">Another action</a></li>
                          <li><a href="javascript:void(0);">Something else here</a></li>
                      </ul>
                  </li>
              </ul>
          </div>
          <div class="body">
              <form action="<?php echo base_url() ?>barang/simpan_barang" method="post">
                <div class="form-group">
                    <label for="email_address">Supplier</label>
                    <div class="form-line">
                      <select class="form-control show-tick" id="supplier" name='supplier' onchange="get_barang(this)">
                          <option value="">Pilih Suplier</option>
                          <?php foreach ($suppliers as $rows) { ?>
                              <?php $i++; ?>
                              <option value="<?php echo $rows['id'] ?>"><?php echo ucwords($rows['nama_supplier']) ?></option>
                          <?php } ?>
                      </select>
                    </div>
                </div>

                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="kode_barang" name="kode_barang" class="form-control" value="">
                          <label class="form-label">Kode Barang</label>
                      </div>
                  </div>

                  <div class="form-group">
                      <label for="email_address">Kategori</label>
                      <div class="form-line">
                        <select class="form-control show-tick" name='kategori'>
                            <option value="">Pilih Kategori</option>
                            <?php foreach ($kategoris as $rows) { ?>
                                <?php $i++; ?>
                                <option value="<?php echo $rows['id'] ?>" ><?php echo ucwords($rows['nama_jenis']) ?></option>
                            <?php } ?>
                        </select>
                      </div>
                  </div>

                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="nama_barang" name="nama_barang" class="form-control" value="">
                          <label class="form-label">Nama Barang</label>
                      </div>
                  </div>

                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="number" id="stok" name="stok" class="form-control" value="">
                          <label class="form-label">Jumlah Stok</label>
                      </div>
                  </div>

                  <div class="form-group">
                      <label for="email_address">Satuan</label>
                      <div class="form-line">
                        <select class="form-control show-tick" name='satuan'>
                            <option value="">Pilih Satuan</option>
                            <?php foreach ($satuans as $rows) { ?>
                                <?php $i++; ?>
                                <option value="<?php echo $rows['id'] ?>" ><?php echo ucwords($rows['nama_satuan']) ?></option>
                            <?php } ?>
                        </select>
                      </div>
                  </div>

                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="number" id="jumlah_stok" name="harga_modal" class="form-control" value="">
                          <label class="form-label">Harga Modal</label>
                      </div>
                  </div>

                  <a href="<?php echo base_url() ?>pembelian" class="btn bg-red btn-lg waves-effect m-t-15 m-r-10">Batal</a>
                  <button type="submit" class="btn bg-blue btn-lg waves-effect m-t-15">Simpan</button>
              </form>
          </div>
      </div>
  </div>
</div>
<!-- Vertical Layout | With Floating Label -->