<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Faktur Pembelian</title>
    
    <style>
    
    @font-face {
        font-family: 'MyWebFont';
        src: url('http://tunasbaruorder.com/assets/font/BOOKOS.TTF'); /* IE9 Compat Modes */
        src: url('http://tunasbaruorder/assets/font/BOOKOS.TTF') format('embedded-opentype'), /* IE6-IE8 */
            url('http://tunasbaruorder/assets/font/BOOKOS.TTF') format('woff2'), /* Super Modern Browsers */
            url('http://tunasbaruorder/assets/font/BOOKOS.TTF') format('woff'), /* Pretty Modern Browsers */
            url('http://tunasbaruorder/assets/font/BOOKOS.TTF')  format('truetype'), /* Safari, Android, iOS */
    }

    @font-face {
        font-family: "Kimberley";
        src: url(http://tunasbaruorder.com/assets/font/BOOKOS.TTF) format("truetype");
    }

    table {
        font-size: 13px;
    }

    td {
        text-align:left;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 1px;
        vertical-align: top;
    }
    
    /* .invoice-box table tr td:nth-child(2) {
        text-align: right;
    } */
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    /* .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    } */
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: left;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }

    .margin-10 {
        margin-top: 30px;
    }

    .logos-tunas {
        font-family: "Kimberley", open-sans;
        margin:0;
        font-size: 20px;
        font-weight:bold;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <div style="display: flex;justify-content: space-between;margin-bottom:50px;align-items:center;">
            <div style="display: flex;align-items: center;">
                <div><img src="<?php echo base_url('assets/images/logo.jpg') ?>" style="width:100%; max-width:80px;"></div>
                <div style="margin-left:10px;font-size:12px;">
                    <span class='logos-tunas'>TUNAS BARU</span><br>
                    <span style="font-size:14px;">Trade & Distribution</span><br>
                    Batipuah Selatan, Tanah Datar <br> Sumatera Barat
                </div>
            </div>
            <div>
                <span style="border-bottom: solid 1px black;font-weight:bold;font-size:18px;">Faktur Penjualan</span>
                <span style="display:block;margin-top:5px;font-size:11px;">No Faktur : <?php echo $header['kode_pesanan']; ?><br></span>
            </div>
            <div style="font-size:14px;">
                Tanggal: <?php echo $header['tanggal_pesanan']; ?><br>
                Nama Toko: <b><?php echo $header['nama_toko']; ?></b><br>
                Daerah : <b><?php echo $header['nama_daerah']; ?></b><br>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="5">Sales: <b><?php echo $header['username']; ?></b></td>
            </tr>
            <tr class="heading">
                <td width="5%">
                    No
                </td>
                <td width="40%">
                    Nama Barang
                </td>
                
                <td width="15%">
                    Qty
                </td>

                <td width="15%">
                    Harga Satuan
                </td>

                <td width="20%">
                    Total
                </td>
            </tr>
            <?php if (!empty($pesanans)) { $i = 0; ?>

                <?php foreach ($pesanans as $rows) { $i++; ?>
                
                    <tr class="item">
                        <td>
                            <?php echo $i; ?>
                        </td>
                        <td>
                            <?php echo $rows['nama_barang'] ?>
                        </td>
                        
                        <td>
                            <?php echo $rows['qty'] ?> <?php echo $rows['nama_satuan'] ?>
                        </td>

                        <td>
                            Rp. <?php echo number_format($rows['harga_satuan'],0,',','.'); ?>
                        </td>

                        <td>
                            Rp. <?php echo number_format($rows['total'],0,',','.'); ?>
                        </td>
                    </tr>

                <?php } ?>
            <?php } else { ?>

                <p>Pembelian Kosong</p>

            <?php } ?>
            
            <tr class="total">
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align: right;"><b>Total:</b></td>
                
                <td>
                    <b>Rp. <?php echo number_format($header['total'],0,',','.'); ?></b>
                </td>
            </tr>
        </table>
        <hr>
        <table style="margin-top:10px;">
            <tr>
                <td style="text-align: center;">
                    Tanda Terima
                </td>
                <td style="text-align: center;">
                    Diketahui
                </td>
                <td style="text-align: center;">
                    Hormat Kami
                </td>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr style="height:30px;"></tr>
            <tr>
                <td style="text-align: center;">
                    (.........................)
                </td>
                <td style="text-align: center;">
                    (.........................)
                </td>
                <td style="text-align: center;">
                    (.........................)
                </td>
            </tr>
        </table>
    </div>

    <script>
        window.print();
    </script>
</body>
</html>
