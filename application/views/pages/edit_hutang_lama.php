<!-- Vertical Layout | With Floating Label -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  Edit Hutang Lama
              </h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="material-icons">more_vert</i>
                      </a>
                      <ul class="dropdown-menu pull-right">
                          <li><a href="javascript:void(0);">Action</a></li>
                          <li><a href="javascript:void(0);">Another action</a></li>
                          <li><a href="javascript:void(0);">Something else here</a></li>
                      </ul>
                  </li>
              </ul>
          </div>
          <div class="body">
              <form action="<?php echo base_url() ?>master/simpan_edit_hutang_lama" method="post">
                  <input type="hidden" name="id" value="<?php echo $id; ?>"> 
                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="jumlah_hutang" name="jumlah_hutang" class="form-control" value="<?php echo $jumlah_hutang; ?>">
                          <label class="form-label">Jumlah Hutang</label>
                      </div>
                  </div>

                  <a href="<?php echo base_url() ?>toko/daftar" class="btn bg-red btn-lg waves-effect m-t-15 m-r-10">Batal</a>
                  <button type="submit" class="btn bg-blue btn-lg waves-effect m-t-15">Simpan</button>
              </form>
          </div>
      </div>
  </div>
</div>
<!-- Vertical Layout | With Floating Label -->