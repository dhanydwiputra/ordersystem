<!-- Vertical Layout | With Floating Label -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  Edit Supplier
              </h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="material-icons">more_vert</i>
                      </a>
                      <ul class="dropdown-menu pull-right">
                          <li><a href="javascript:void(0);">Action</a></li>
                          <li><a href="javascript:void(0);">Another action</a></li>
                          <li><a href="javascript:void(0);">Something else here</a></li>
                      </ul>
                  </li>
              </ul>
          </div>
          <div class="body">
              <form action="<?php echo base_url() ?>supplier/simpan_edit_supplier" method="post">
                  <input type="hidden" name="id" value="<?php echo $id; ?>">
                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="nama_supplier" name="nama_supplier" class="form-control" value="<?php echo $nama_supplier; ?>">
                          <label class="form-label">Nama Supplier</label>
                      </div>
                  </div>

                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="telepon_supplier" name="telepon_supplier" class="form-control" value="<?php echo $telepon_supplier; ?>">
                          <label class="form-label">Telepon Supplier</label>
                      </div>
                  </div>

                  <a href="<?php echo base_url() ?>supplier/daftar_supplier" class="btn bg-red btn-lg waves-effect m-t-15 m-r-10">Batal</a>
                  <button type="submit" class="btn bg-blue btn-lg waves-effect m-t-15">Simpan</button>
              </form>
          </div>
      </div>
  </div>
</div>
<!-- Vertical Layout | With Floating Label -->