<!-- Vertical Layout | With Floating Label -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <div class="header">
              <h2>
                  Bayar Hutang
              </h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="material-icons">more_vert</i>
                      </a>
                      <ul class="dropdown-menu pull-right">
                          <li><a href="javascript:void(0);">Action</a></li>
                          <li><a href="javascript:void(0);">Another action</a></li>
                          <li><a href="javascript:void(0);">Something else here</a></li>
                      </ul>
                  </li>
              </ul>
          </div>
          <div class="body">
              <form action="<?php echo base_url() ?>hutang/simpan_pembayaran" method="post">

                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="nomor_referensi" name="nomor_referensi" class="form-control" value="<?php echo $header['no_referensi']; ?>" readonly>
                          <label class="form-label">Nomor Referensi</label>
                      </div>
                  </div>

                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="nama_supplier" name="nama_supplier" class="form-control" value="<?php echo $header['nama_supplier']; ?>" readonly>
                          <label class="form-label">Nama Supplier</label>
                      </div>
                  </div>

                  <div class="form-group">
                      <label for="email_address">Tanggal Pembayaran</label>
                      <div class="form-line">
                          <input type="text" id="tanggal" name="tanggal" class="datepicker form-control" placeholder="Please choose a date..." required>
                      </div>
                  </div>

                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="total_pembayaran" name="total_pembayaran" class="form-control" value="<?php echo $header['total']; ?>" readonly>
                          <label class="form-label">Total Pembayaran</label>
                      </div>
                  </div>

                  <div class="form-group form-float">
                      <div class="form-line">
                          <input type="text" id="jumlah_bayar" name="jumlah_bayar" class="form-control" value="" required>
                          <label class="form-label">Jumlah dibayar</label>
                      </div>
                  </div>

                  <a href="<?php echo base_url() ?>hutang/detail/<?php echo $header['no_referensi'];  ?>" class="btn bg-red btn-lg waves-effect m-t-15 m-r-10">Batal</a>
                  <button type="submit" class="btn bg-blue btn-lg waves-effect m-t-15">Simpan</button>
              </form>
          </div>
      </div>
  </div>
</div>
<!-- Vertical Layout | With Floating Label -->

<script>
  $('.datepicker').bootstrapMaterialDatePicker({
      format: 'YYYY-MM-DD',
      clearButton: true,
      weekStart: 1,
      time: false
  });
</script>