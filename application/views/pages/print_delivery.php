<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Faktur Pembelian</title>
    
    <style>

    table {
        font-size: 13px;
    }

    @page { size: auto;  margin: 5mm; }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 1px;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    /* .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    } */
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: left;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }

    .margin-10 {
        margin-top: 30px;
    }

    .left {
        float: left;
    }

    .right {
        float: right;
    }

    .wrapper {
        border: 2px dotted rgb(96, 139, 168);
        width: 500px;
      }
    .box {

        display: flex;
        flex-wrap: wrap;
        margin:-10px;
      }
      .box>* {
        flex: 1 1 160px;
        margin: 10px;
      }

      .td-nobox {
        border-bottom : none !important;
      }
      .logos-tunas {
        font-family: "Kimberley", open-sans;
        margin:0;
        font-size: 20px;
        font-weight:bold;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <div style="display: flex;justify-content: space-between;margin-bottom:50px;align-items:center;">
            <div style="display: flex;align-items: center;">
                <div><img src="<?php echo base_url('assets/images/logo.jpg') ?>" style="width:100%; max-width:80px;"></div>
                <div style="margin-left:10px;font-size:12px;">
                    <span class='logos-tunas'>TUNAS BARU</span><br>
                    <span style="font-size:14px;">Trade & Distribution</span><br>
                    Batipuah Selatan, Tanah Datar <br> Sumatera Barat
                </div>
            </div>
            <div style="font-size:14px;">
                Nomor DO : <?php echo $header['no_do']; ?><br>
                Driver: <?php echo $header['driver']; ?><br>
                Tanggal: <?php echo $header['tanggal']; ?><br>
                No Gudang: <?php echo $header['no_gudang']; ?><br>
                No Kendaraan: <?php echo $header['no_kendaraan']; ?>
            </div>
        </div>
        <div style="margin-bottom:30px;">
          <hr>
        </div>
        <div>
            <table border="1" style="border-collapse: collapse;">
                <tr class="heading">
                    <td>
                        Daerah
                    </td>
                    
                    <td>
                        <table>
                          <td class="td-nobox">Nama Barang</td>
                          <td class="td-nobox">
                              Qty
                          </td>
                        </table>
                    </td>
                </tr>
                <?php $last_name=""; ?>
                <?php if (!empty($pesanans)) { $i = 0; ?>

                    <?php foreach ($pesanans as $rows) { ?>
                        <!-- <tr class="item">
                            <td>
                                <?php echo $rows['nama_daerah'] ?>
                            </td>
                            
                            <td>
                                <?php echo $rows['nama_barang'] ?>
                            </td>

                            <td>
                            <?php echo $rows['qty'] ?>
                            </td>
                        </tr> -->

                        <?php 
                          if($last_name != $rows['nama_daerah'])
                          {
                            if($last_name != "")
                            {
                              echo "</table></tr>";
                            }
                            echo "<tr>
                            <td>".$rows['nama_daerah']."</td>
                            <td><table>";
                          }
                          echo "
                            <tr>
                              <td>".$rows['nama_barang']."</td>
                              <td>".$rows['qty']."</td>
                            </tr>
                          ";
                          if($last_name != $rows['nama_daerah'])
                          {
                            $last_name = $rows['nama_daerah'];
                          }
                        ?>

                    <?php } ?>
                <?php } else { ?>

                    <p>Pembelian Kosong</p>

                <?php } ?>
            </table>
        </div>

    </div>

    <script>
        window.print();
    </script>
</body>
</html>
