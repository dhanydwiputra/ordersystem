 <style>
  #navbar-collapse > ul > li.dropdown.open > ul > li.body > ul {
    list-style: none;
  }

  .status-online {
      width: 10px;
      height: 10px;
      background-color: #63d863;
      border-radius: 50%;
      position: absolute;
      top: 20px;
  }

  .status-offline {
      width: 10px;
      height: 10px;
      background-color: black;
      border-radius: 50%;
      position: absolute;
      top: 20px;
  }

  .logo-relative {
      position: relative;
  }
 </style>
 
<script>
    setInterval(function () {
        var status = document.getElementById('status-network')
        status.className = navigator.onLine ? 'status-online' : 'status-offline';
    }, 100);
</script>

 <!-- Page Loader -->
 <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <div class="logo-relative">
                    <a class="navbar-brand" href="index.html">Tunas Baru</a>
                    <span id="status-network"></span>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->