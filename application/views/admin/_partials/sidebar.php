<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="<?php echo base_url('assets/images/user.png') ?>" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php $userid = $this->session->userdata('login'); echo ucfirst($userid['username']); ?></div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="<?php echo base_url() ?>login/logout"><i class="material-icons">input</i>Log Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">Menu</li>
                <!-- <li>
                    <a href="index.html">
                        <i class="material-icons">home</i>
                        <span>Dashboard</span>
                    </a>
                </li> -->
                <li class="<?php if ($root_menu == "barang") echo 'active'; ?> <?php if ($this->session->userdata['login']['role'] == 2) echo 'hide'; ?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">archive</i>
                        <span>Manajemen Barang</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="<?php if ($segment == "barang") echo 'active'; ?>">
                            <a href="<?php echo base_url() ?>barang">Lihat Data Barang</a>
                        </li>
                        <li class="<?php if ($segment == "pembelian") echo 'active'; ?>">
                            <a href="<?php echo base_url() ?>pembelian">Pembelian Barang</a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($root_menu == "toko") echo 'active'; ?> <?php if ($this->session->userdata['login']['role'] == 2) echo 'hide'; ?>">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">account_balance</i>
                        <span>Manajemen Toko-Toko</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="<?php if ($segment == "daftar_toko") echo 'active'; ?>">
                            <a href="<?php echo base_url() ?>toko/daftar">Daftar Toko</a>
                        </li>
                        <!-- <li>
                            <a href="pages/ui/alerts.html">Tambah Daerah</a>
                        </li> -->
                    </ul>
                </li>
                <li class=<?php if ($root_menu == "pesanan") echo 'active'; ?>>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">reorder</i>
                        <span>Manajemen Pesanan</span>
                    </a>
                    <ul class="ml-menu">
                        <li class=<?php if ($segment == "add_pesanan") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>pesanan">Tambah Pesanan</a>
                        </li>
                        <li class=<?php if ($segment == "show_pesanan") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>pesanan/show_pesanan">Lihat Pesanan</a>
                        </li>
                    </ul>
                </li>
                <li class=<?php if ($root_menu == "piutang") echo 'active'; ?>>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">monetization_on</i>
                        <span>Manajemen Piutang</span>
                    </a>
                    <ul class="ml-menu">
                        <li class = <?php if ($segment == "daftar_piutang") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>piutang">Lihat Piutang</a>
                        </li>
                        <li class = <?php if ($segment == "bayar_piutang") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>piutang/bayar">Bayar Piutang</a>
                        </li>
                    </ul>
                </li>
                <li class=<?php if ($root_menu == "hutang") echo 'active'; ?> <?php if ($this->session->userdata['login']['role'] == 2) echo 'hide'; ?>>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">monetization_on</i>
                        <span>Manajemen Hutang</span>
                    </a>
                    <ul class="ml-menu">
                        <li class = <?php if ($segment == "show_hutang") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>hutang/show">Lihat Hutang</a>
                        </li>
                        <li class = <?php if ($segment == "list_supplier") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>supplier/daftar_supplier">List Supplier</a>
                        </li>
                    </ul>
                </li>
                <li class=<?php if ($root_menu == "delivery") echo 'active'; ?> <?php if ($this->session->userdata['login']['role'] == 2) echo 'hide'; ?>>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">monetization_on</i>
                        <span>Manajemen Delivery Order</span>
                    </a>
                    <ul class="ml-menu">
                        <li class = <?php if ($segment == "list_do") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>delivery/list_do">Lihat DO</a>
                        </li>
                        <li class = <?php if ($segment == "show_do") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>delivery">Buat Delivery Order</a>
                        </li>
                    </ul>
                </li>
                <li class=<?php if ($root_menu == "master") echo 'active'; ?> <?php if ($this->session->userdata['login']['role'] == 2) echo 'hide'; ?>>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">assignment_ind</i>
                        <span>Master Data</span>
                    </a>
                    <ul class="ml-menu">
                        <li class = <?php if ($segment == "daftar_user") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>master/user">Master User</a>
                        </li>
                        <li class = <?php if ($segment == "satuan") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>master/satuan">Master Satuan Barang</a>
                        </li>
                        <li class = <?php if ($segment == "kategori") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>master/kategori">Master Kategori</a>
                        </li>
                        <li class = <?php if ($segment == "daerah_user") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>master/daerah">Master Daerah</a>
                        </li>
                        <li class = <?php if ($segment == "hutang_lama") echo 'active'; ?>>
                            <a href="<?php echo base_url() ?>master/hutang_lama">Hutang Lama</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2018 - 2019 <a href="javascript:void(0);">Tunas Baru Corp</a>.
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>