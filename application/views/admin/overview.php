<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body class="theme-pink">
  <?php $this->load->view("admin/_partials/header.php") ?>
  <?php $this->load->view("admin/_partials/sidebar.php") ?>
  <?php $this->load->view("admin/_partials/js.php") ?>

   <section class="content">
        <div class="container-fluid">
            <?php $this->load->view($pages) ?>
        </div>
    </section>
</body>